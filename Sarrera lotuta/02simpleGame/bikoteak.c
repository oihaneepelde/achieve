#define _CRT_SECURE_NO_WARNINGS
#include "bikoteak.h"
#include "sarrera.h"
#include "letrak.h"
#include "zenbakiak.h"
#include "koloreak.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "desberdintasunak.h"
#include "text.h"
#include "soinua.h"
#include "stdlib.h"
#include "time.h"
#include <stdio.h>
#include <windows.h>

typedef struct S_GURE_GAUZAK
{
    int idIrudi, idMenu, idMapa, idAzalpenak, idKredituak, idKartak, idSound, idKarta;

}GURE_GAUZAK;
typedef struct coordenadas
{
    int x;
    int y;
    int rx;
    int ry;
}coordenadas;
typedef struct randompos
{
    int random;
    int x;
    int K;
}RANDOMPOS;
GURE_GAUZAK gureGauzak;
coordenadas lista[3];
RANDOMPOS listar[6];

//aldagai globalak
int kokapenax[8] = { 46,186,332,475,623,773 };
int kokapenay[2] = { 180,430 };
int errepikatuak[3];
int behin[17];
int x, p, h, z, puntuak, j, m, aurkituta;
void Ondosum()
{
    int idSound;
    Mix_Music* musika = NULL;

    audioInit();
    idSound = loadSound(ONDO);
    playSound(idSound);

}
void Gaizkisum()
{
    int idSound;
    Mix_Music* musika = NULL;

    audioInit();
    idSound = loadSound(GAIZKI);
    playSound(idSound);

}
//Izenburuaren pantaila---------------------------------------------------------------------------------------------------------------------------------------
void sumhasiera()
{
    int ebentu = 0, kont = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(FONDO_SUMENDI);//Atzeko planoa
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        //Aurrera jarraitzeko botoia
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 127 && arratoiarenPos.x < 860 && arratoiarenPos.y>270 && arratoiarenPos.y < 461)
        {
            arauaks();
        }
        //Mapara itzultzeeko botoia
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 806 && arratoiarenPos.x < 916 && arratoiarenPos.y>580 && arratoiarenPos.y < 674)
        {
            atzera();
        }
    } while (egoera == JOLASTEN);
}
//Irlako pertsonaiaren aurkezpena-----------------------------------------------------------------------------------------------------------------------------
void arauaks()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\istorioa2.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        //Aurrera jarraitzeko botoia
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 226 && arratoiarenPos.x < 335 && arratoiarenPos.y>493 && arratoiarenPos.y < 580)
        {
            arauaks2();
        }
    } while (egoera == JOLASTEN);

}
//Jokoaren arauak---------------------------------------------------------------------------------------------------------------------------------------------
void arauaks2()

{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\arauaksum.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        //Jokoa hasteko botoia
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 340 && arratoiarenPos.x < 520 && arratoiarenPos.y>618 && arratoiarenPos.y < 685)
        {
            bikoteakaurkitu();
        }
    } while (egoera == JOLASTEN);
}
//Jokoaren funtzio nagusia------------------------------------------------------------------------------------------------------------------------------------
void bikoteakaurkitu()
{
    x = 0, j = 0, z = 0, p = 0, h = 0, puntuak = 0, aurkituta = 0;
    int ebentu = 0, ebentukop = 0, aurkituta = 0, K1 = 0, K2 = 0, K3 = 0, K4 = 0, K5 = 0, K6 = 0, bikoiti = 0, pos = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;

    int irudiak[] = { KARTA_1, KARTA_2, KARTA_3 };
    int luzeera = sizeof(irudiak) / sizeof(*irudiak);//random egiteko zenbaki tartea aldagai batean gorde
    int random;
    char str[128];
    int err = 0, i, l = 0, k;
    for (i = 0; i < 6; i++) listar[i].random = 2;// zerrendak 0 ez den zenbaki batekin hasieratu behar dira 0 randomean erabiltzen dugulako
    for (i = 0; i < 3; i++) errepikatuak[i] = 3;
    for (i = 0; i < 17; i++) behin[i] = 3;
    pantailaGarbitu();
    gureGauzak.idAzalpenak = irudiaKargatu(NIBELA1);//Atzeko planoaren kokapena
    irudiakMarraztu();
    kartakmarraztu();
    textuNormala();
    textuaIdatzizuriz(700, 50, "PUNTUAK:");//Jokora sartu orduko PUNTUAK:0 agertzeko
    sprintf(str, "%d", puntuak);
    textuaIdatzizuriz(800, 50, str);
    irudiakMarraztu();
    pantailaBerriztu();
    textuaDesgaitu();
    //Koordenatuen buklea (Jolasten ari garen bitartean karta batean klik eginez gero, detektatu egingo du)
    while (ebentukop <= 100 && egoera == JOLASTEN)
    {

        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        //Karta bakoitzaren koordenatuak
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 207 && arratoiarenPos.x < 327 && arratoiarenPos.y>180 && arratoiarenPos.y < 317)
        {
            //Lehen aldiz karta honetan klik egitean gertatuko dena
            if (K1 == 0)
            {
                srand((unsigned int)time(NULL));
                random = rand() % (luzeera);// zenbaki bat aukeratu [0-2] tartean
                //Zenbaki hori lehenago agertu bada beste bat aukeratu
                for (i = 0; i < 2; i++)
                {
                    err = errepikatuak[i];
                    while (err == random || errepikatuak[0] == random)
                    {
                        random = rand() % (luzeera);
                    }
                }
                //Aukeratutako zenbakia lista batean gorde, bere posizioa eta karta zenbakiarekin batera
                listar[j].x = x;
                listar[j].K = 1;
                listar[j].random = random;
                j++;
            }
            //Lehenago karta honetan klik egin badugu gertatuko dena
            else
            {
                pos = 0;
                while (listar[pos].K != 1)pos++;//Gure listan karta zenbakia aurkitu eta bere posizioa gorde
                x = pos;
                m = 1;
            }
            if (m == 0) x = j - 1;
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);//Irudiak listatik dagokion kartaren irudia aukeratu eta kokatu
            irudiaMugitu(gureGauzak.idKarta, 207, 180);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;//Egindako klik kopurua
            K1++;//Konkretuki karta honetan zenbat aldiz klikatu dugun

        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 416 && arratoiarenPos.x < 536 && arratoiarenPos.y>180 && arratoiarenPos.y < 317)
        {
            if (K2 == 0)
            {
                srand((unsigned int)time(NULL));
                random = rand() % (luzeera);
                for (i = 0; i < 2; i++)
                {
                    err = errepikatuak[i];
                    while (err == random || errepikatuak[0] == random)
                    {
                        random = rand() % (luzeera);
                    }
                }
                listar[j].x = x;
                listar[j].K = 2;
                listar[j].random = random;
                j++;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 2)pos++;
                x = pos;
                m = 1;
            }
            if (m == 0) x = j - 1;
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, 416, 180);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K2++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 640 && arratoiarenPos.x < 760 && arratoiarenPos.y>180 && arratoiarenPos.y < 317)
        {
            if (K3 == 0)
            {
                srand((unsigned int)time(NULL));
                random = rand() % (luzeera);
                for (i = 0; i < 2; i++)
                {
                    err = errepikatuak[i];
                    while (err == random || errepikatuak[0] == random)
                    {
                        random = rand() % (luzeera);
                    }
                }
                listar[j].x = x;
                listar[j].K = 3;
                listar[j].random = random;
                j++;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 3)pos++;
                x = pos;
                m = 1;
            }
            if (m == 0) x = j - 1;
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, 640, 180);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K3++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 207 && arratoiarenPos.x < 327 && arratoiarenPos.y>430 && arratoiarenPos.y < 567)
        {
            if (K4 == 0)
            {
                srand((unsigned int)time(NULL));
                random = rand() % (luzeera);
                for (i = 0; i < 2; i++)
                {
                    err = errepikatuak[i];
                    while (err == random || errepikatuak[0] == random)
                    {
                        random = rand() % (luzeera);
                    }
                }
                listar[j].x = x;
                listar[j].K = 4;
                listar[j].random = random;
                j++;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 4)pos++;
                x = pos;
                m = 1;
            }
            if (m == 0) x = j - 1;
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, 207, 430);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K4++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 416 && arratoiarenPos.x < 536 && arratoiarenPos.y>430 && arratoiarenPos.y < 567)
        {
            if (K5 == 0)
            {
                srand((unsigned int)time(NULL));
                random = rand() % (luzeera);
                for (i = 0; i < 2; i++)
                {
                    err = errepikatuak[i];
                    while (err == random || errepikatuak[0] == random)
                    {
                        random = rand() % (luzeera);
                    }
                }
                listar[j].x = x;
                listar[j].K = 5;
                listar[j].random = random;
                j++;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 5)pos++;
                x = pos;
                m = 1;
            }
            if (m == 0) x = j - 1;
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, 416, 430);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K5++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 640 && arratoiarenPos.x < 760 && arratoiarenPos.y>430 && arratoiarenPos.y < 567)
        {
            if (K6 == 0)
            {
                srand((unsigned int)time(NULL));
                random = rand() % (luzeera);
                for (i = 0; i < 2; i++)
                {
                    err = errepikatuak[i];
                    while (err == random || errepikatuak[0] == random)
                    {
                        random = rand() % (luzeera);
                    }
                }
                listar[j].x = x;
                listar[j].K = 6;
                listar[j].random = random;
                j++;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 6)pos++;
                x = pos;
                m = 1;
            }
            if (m == 0) x = j - 1;
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, 640, 430);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K6++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 24 && arratoiarenPos.x < 186 && arratoiarenPos.y>635 && arratoiarenPos.y < 693)
        {
            atzera();
        }
        textuNormala();
        textuaIdatzizuriz(700, 50, "PUNTUAK:");//Puntuak ez desagertzeko
        sprintf(str, "%d", puntuak);
        textuaIdatzizuriz(850, 50, str);
        pantailaBerriztu();
        textuaDesgaitu();
        
        bikoiti = bikoitia(ebentukop);//Klik kopurua bakoitia den edo ez jakiteko funtzioa
        //Buelta bakoitzeko lehen kartan klik egitean gertatuko dena
        if (bikoiti != 1 && ebentu == SAGU_BOTOIA_EZKERRA)
        {
            //if (m == 0) j++;
            l = x;//Karta honen posizioa gorde aldagai finko batean 
            x++;//Hurrengo kartaren posizioa jasotzeko prestatu
            behin[p] = listar[l].random;
            k = zenbatAldiz(behin, 17, behin[p]);
            if (k == 2)
            {
                errepikatuak[z] = listar[l].random;
                z++;
            }
            p++;

        }
       
        //Bigarren kartan kilk egitean gertatuko dena
        if ((bikoiti == 1 && ebentukop != 0) && ebentu == SAGU_BOTOIA_EZKERRA)
        {
            //Bi kartak berdinak edo ezberdinak diren ikusi 
            if (listar[x].random == listar[l].random)
            {
                egoera = zuzena(l);//Berdinak direnean funtzio honetara joango dira
                //Irabaztean funtzio honek egoera aldatzeko ere balioko digu
            }

            else
            {
                gaizki(l);//Ezberdinak direnean funtzio honetara joango dira
            }
        }
    }
    if (egoera == IRABAZI)
    {
        //Jokoa irabazitakoan agertuko diren atzeko planoa, textua eta puntuazioa
        gureGauzak.idAzalpenak = irudiaKargatu(HURRENGONIBELA);
        irudiaMugitu(gureGauzak.idAzalpenak, 746, 630);
        irudiakMarraztu();
        pantailaBerriztu();
        Subtitulo();
        textuaIdatzizuriz(300, 350, "IRABAZI DUZU!!");
        textuNormala();
        textuaIdatzizuriz(700, 50, "PUNTUAK:");
        sprintf(str, "%d", puntuak);
        textuaIdatzizuriz(850, 50, str);
        pantailaBerriztu();
        //Hurrengo pantailara igarotzeko botoia
        do
        {
            ebentu = ebentuaJasoGertatuBada();
            arratoiarenPos = saguarenPosizioa();
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 746 && arratoiarenPos.x < 900 && arratoiarenPos.y>630 && arratoiarenPos.y < 660)
            {
                samaiera(puntuak);
            }
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 24 && arratoiarenPos.x < 186 && arratoiarenPos.y>635 && arratoiarenPos.y < 693)
            {
                atzera();
            }
        } while (egoera == IRABAZI);
    }

}
/*void bikoteakaurkitu2(int puntuak1)
{
    int ebentu = 0, ebentukop = 0, x = 0, aurkituta = 0, K1 = 0, K2 = 0, K3 = 0, K4 = 0, K5 = 0, K6 = 0, bikoiti = 0, pos = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    RANDOMPOS listar[12];
    int irudiak[] = { KARTA_1, KARTA_2, KARTA_3, KARTA_4, KARTA_5, KARTA_6 };
    int luzeera = sizeof(irudiak) / sizeof(*irudiak);
    int random;
    int errepikatuak[6];
    int behin[14];
    char str[128];
    int kartax = 120;
    int kartay = 137;
    int err = 0, i, puntuak2 = 0, j = 0, k = 0, l = 0, p = 0, h = 0,kont=0;
    for (i = 0; i < 6; i++) listar[i].random = 8;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idAzalpenak);
    irudiaKendu(gureGauzak.idKarta);
    irudiaKendu(gureGauzak.idKartak);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\fondofuego2.0.bmp");
    irudiakMarraztu();
    kartakmarraztu2();
    textuNormala();
    textuaIdatzizuriz(750, 50, "PUNTUAK:");
    sprintf(str, "%d", puntuak2);
    textuaIdatzizuriz(850, 50, str);
    irudiakMarraztu();
    pantailaBerriztu();
    while (ebentukop <= 100 && egoera == JOLASTEN)
    {

        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[0] && arratoiarenPos.x < kokapenax[0]+kartax && arratoiarenPos.y>kokapenay[0] && arratoiarenPos.y < kokapenay[0] +kartay)
        {
            if (K1 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 1;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 1)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[0], kokapenay[0]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K1++;

        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[1] && arratoiarenPos.x < kokapenax[1]+kartax && arratoiarenPos.y>kokapenay[0] && arratoiarenPos.y < kokapenay[0]+kartay)
        {
            if (K2 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 2;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 2)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[1], kokapenay[0]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K2++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[2] && arratoiarenPos.x < kokapenax[2]+kartax && arratoiarenPos.y>kokapenay[0] && arratoiarenPos.y < kokapenay[0]+kartay)
        {
            if (K3 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 3;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 3)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[2], kokapenay[0]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K3++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[3] && arratoiarenPos.x < kokapenax[3]+kartax && arratoiarenPos.y>kokapenay[0] && arratoiarenPos.y < kokapenay[0]+kartay)
        {
            if (K4 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 4;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 4)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[3], kokapenay[0]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K4++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[4] && arratoiarenPos.x < kokapenax[4]+kartax && arratoiarenPos.y>kokapenay[0] && arratoiarenPos.y < kokapenay[0]+kartay)
        {
            if (K5 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 5;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 5)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[4], kokapenay[0]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K5++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[5] && arratoiarenPos.x < kokapenax[5]+kartax && arratoiarenPos.y>kokapenay[0] && arratoiarenPos.y < kokapenay[0] + kartay)
        {
            if (K6 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 6;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 6)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[5], kokapenay[0]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K6++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[0] && arratoiarenPos.x < kokapenax[0]+kartax && arratoiarenPos.y>kokapenay[1] && arratoiarenPos.y < kokapenay[1] + kartay)
        {
            if (K1 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 1;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 1)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[0], kokapenay[1]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K1++;

        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[1] && arratoiarenPos.x < kokapenax[1]+kartax && arratoiarenPos.y>kokapenay[1] && arratoiarenPos.y < kokapenay[1] + kartay)
        {
            if (K2 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 2;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 2)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[1], kokapenay[1]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K2++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[2] && arratoiarenPos.x < kokapenax[2]+kartax && arratoiarenPos.y>kokapenay[1] && arratoiarenPos.y < kokapenay[1] + kartay)
        {
            if (K3 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 3;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 3)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[2], kokapenay[1]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K3++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[3] && arratoiarenPos.x < kokapenax[3]+kartax && arratoiarenPos.y>kokapenay[1] && arratoiarenPos.y < kokapenay[1] + kartay)
        {
            if (K4 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 4;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 4)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[3], kokapenay[1]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K4++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[4] && arratoiarenPos.x < kokapenax[4]+kartax && arratoiarenPos.y>kokapenay[1] && arratoiarenPos.y < kokapenay[1] + kartay)
        {
            if (K5 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 5;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 5)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[4], kokapenay[1]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K5++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > kokapenax[5] && arratoiarenPos.x < kokapenax[5]+kartax && arratoiarenPos.y>kokapenay[1] && arratoiarenPos.y < kokapenay[1] + kartay)
        {
            if (K6 == 0)
            {
                srand(time(NULL));
                random = rand() % (luzeera);
                listar[j].x = x;
                listar[j].K = 6;
                listar[j].random = random;
            }
            else
            {
                pos = 0;
                while (listar[pos].K != 6)pos++;
                x = listar[pos].x;

            }
            gureGauzak.idKarta = irudiaKargatu(irudiak[listar[x].random]);
            irudiaMugitu(gureGauzak.idKarta, kokapenax[5], kokapenay[1]);
            irudiakMarraztu();
            pantailaBerriztu();
            ebentukop++;
            K6++;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 24 && arratoiarenPos.x < 186 && arratoiarenPos.y>635 && arratoiarenPos.y < 693)
        {
            hasiu(kont);
        }

        bikoiti = bikoitia(ebentukop);
        if (bikoiti != 1 && ebentu == SAGU_BOTOIA_EZKERRA && x < 5)
        {
            j++;
            l = x;
            x++;

        }
        textuNormala();
        textuaIdatzizuriz(750, 50, "PUNTUAK:");
        sprintf(str, "%d", puntuak2);
        textuaIdatzizuriz(900, 50, str);
        pantailaBerriztu();

        if ((bikoiti == 1) && ebentu == SAGU_BOTOIA_EZKERRA)
        {

            if (listar[x].random == listar[l].random)
            {
                for (i = 0; i < 3; i++)
                {
                    while (listar[x].random == errepikatuak[i] && i < 2)
                    {
                        gureGauzak.idTextua = irudiaKargatu(".\\img\\errep.bmp");
                        irudiaMugitu(gureGauzak.idTextua, 167, 357);
                        irudiakMarraztu();
                        pantailaBerriztu();
                        h = 1;
                    }
                }

                if (h==2)
                {
                    aurkituta++;
                    gureGauzak.idTextua = irudiaKargatu(".\\img\\bikotebatasmatuu.bmp");
                    irudiaMugitu(gureGauzak.idTextua, 167, 357);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    errepikatuak[k] = listar[x].random;
                    k++;
                    puntuak2 = puntuak2 + 10;
                }
                egoera = AZTERTZEN;
                do
                {
                    ebentu = ebentuaJasoGertatuBada();
                    if (ebentu == TECLA_RIGHT)
                    {
                        irudiaKendu(gureGauzak.idTextua);
                        irudiakMarraztu();
                        pantailaBerriztu();
                        egoera = JOLASTEN;
                    }
                } while (egoera == AZTERTZEN);

                if (aurkituta == 6)
                {
                    egoera = IRABAZI;

                }
            }

            else
            {

                gureGauzak.idTextua = irudiaKargatu(".\\img\\bikoteaezasmatu.bmp");
                irudiaMugitu(gureGauzak.idTextua, 186, 320);
                irudiakMarraztu();
                pantailaBerriztu();
                //behin[p] = listar[l].random;
                //behin[p + 1] = listar[x].random;
                //p=p+2;

                if (puntuak2 <= 0) puntuak2 = 0;
                else puntuak2 = puntuak2 - 3;
                egoera = AZTERTZEN;
                do
                {
                    ebentu = ebentuaJasoGertatuBada();
                    if (ebentu == TECLA_RIGHT)
                    {

                        pantailaGarbitu();
                        irudiaKendu(gureGauzak.idTextua);
                        gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\fondofuego2.bmp");
                        irudiakMarraztu();
                        kartakmarraztu2();
                        pantailaBerriztu();
                        aurkituta = 0;
                        egoera = JOLASTEN;
                    }
                } while (egoera == AZTERTZEN);
            }
            if (x < 5)
            {
                j++;
                x++;
            }
            else if (x == 6) x = 0;
            egoera = IRABAZI;
        }
    }
    if (egoera == IRABAZI)
    {
        gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\hurrengonibela.bmp");
        irudiaMugitu(gureGauzak.idAzalpenak, 746, 630);
        irudiakMarraztu();
        pantailaBerriztu();
        Subtitulo();
        textuaIdatzizuriz(300, 350, "IRABAZI DUZU!!");
        pantailaBerriztu();
        puntuak2 = puntuak1 + puntuak2;
        do
        {
            ebentu = ebentuaJasoGertatuBada();
            arratoiarenPos = saguarenPosizioa();
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 746 && arratoiarenPos.x < 900 && arratoiarenPos.y>630 && arratoiarenPos.y < 660)
            {
                samaiera(puntuak2);
            }
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 24 && arratoiarenPos.x < 186 && arratoiarenPos.y>635 && arratoiarenPos.y < 693)
            {

                hasiu(kont);
            }
        } while (egoera == IRABAZI);
    }

}*/
//Karta guztiak koordenatu finkoetan kokatzen dituen funtzioa---------------------------------------------------------------------------------------------
void kartakmarraztu()
{
    int i = 0, j = 0, horizontala = 0, bertikala = 0, kont = 0;
    EGOERA  egoera = JOLASTEN;
    //POSIZIOA arratoiarenPos;
    int x[3] = { 207,416,640 };//Karten koordenatu finkoak x ardatzean
    int y[2] = { 180,430 };//Karten koordenatu finkoak y ardatzean
    while ((i < 3) || (j < 1))
    {
        if (kont == 3)
        {
            i = 0;
            j++;
        }
        horizontala = x[i];
        i++;
        kont++;
        bertikala = y[j];
        gureGauzak.idKartak = irudiaKargatu(".\\img\\kartaa.bmp");
        irudiaMugitu(gureGauzak.idKartak, horizontala, bertikala);
        irudiakMarraztu();
    }
}
/*void kartakmarraztu2()
{
    int i = 0, j = 0, horizontala = 0, bertikala = 0, kont = 0, kartakop;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    kartakop = 6;
    int kartax[8] = { 46,186,332,475,623,773 };
    int kartay[2] = { 180,430 };
    while ((i < kartakop) || (j < 1))
    {
        if (kont == kartakop)
        {
            i = 0;
            j++;
        }
        horizontala = kartax[i];
        i++;
        kont++;
        bertikala = kartay[j];
        gureGauzak.idKartak = irudiaKargatu(".\\img\\kartaa.bmp");
        irudiaMugitu(gureGauzak.idKartak, horizontala, bertikala);
        irudiakMarraztu();
    }
}*/
//Zenbaki bat bikoitia den edo ez erabitzen dugun funtzioa------------------------------------------------------------------------------------------------
int bikoitia(int z)
{
    int bik = 0;
    if (z % 2 == 0)
    {
        bik = 1;
    }
    return bik;
}
//Elementu bat array batean zenbat aldiz dagoen jakiteko funtzioa----------------------------------------------------------------------------------------
int zenbatAldiz(int b[], int dim, int elementu)
{
    int i, kont = 0;
    for (i = 0; i < dim; i++)
    {
        if (b[i] == elementu) kont++;

    }
    return kont;
}
int zenbatAldiz2(int dim, int elementu)
{
    int i, kont = 0;
    for (i = 0; i < dim; i++)
    {
        if (listar[i].K == elementu) kont++;

    }
    return kont;
}
//Jokoa amaitutakoan agertzen den azken pantaila---------------------------------------------------------------------------------------------------------
void samaiera(int puntuak)
{
    int ebentu = 0, kont = 1;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    char str[128];
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idAzalpenak);
    irudiaKendu(gureGauzak.idKarta);
    irudiaKendu(gureGauzak.idKartak);
    gureGauzak.idAzalpenak = irudiaKargatu(IRABAZITA);
    irudiakMarraztu();
    pantailaBerriztu();
    Subtitulo();
    sprintf(str, "%d", puntuak);
    textuaIdatzibeltzez(400, 300, str);
    //irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 315 && arratoiarenPos.x < 605 && arratoiarenPos.y>456 && arratoiarenPos.y < 529)
        {
            bostgarrenuhartea();
        }
    } while (egoera == JOLASTEN);
}
//Bi karta berdinak direnean erabiltzen duguna-----------------------------------------------------------------------------------------------------------
int zuzena(int l)
{
    EGOERA egoera = JOLASTEN;
    int k = 0, ebentu = 0;
    
    k = zenbatAldiz2(6, listar[x].K);
    //Bi aldiz karta berdinean klikatuz gero----------------------------------------
    if (listar[x].K == listar[l].K)
    {
        behin[p] = listar[x].random;
        p++;
        m = 1;

    }
    //Karta ezberdinak izan arren bikote berekoak direnean-------------------------------
    else
    {
        /*if (k==0||(h == 1))
        {*/
        aurkituta++;
        puntuak = puntuak + 10;
        behin[p] = listar[x].random;
        k = zenbatAldiz(behin, 17, behin[p]);
        if (k == 2)
        {
            errepikatuak[z] = listar[x].random;
            z++;
        }
        p++;
        h = 0;
        x++;
        if (m == 1) x=x+2;
        //}
        Subtitulo();
        textuaIdatzizuriz(145, 357, "BIKOTE BAT ASMATU DUZUU!!");
        Ondosum();
        pantailaBerriztu();
        textuaDesgaitu();
        egoera = AZTERTZEN;
        m = 0;
    }
    //Jokatzen jarraitzeko ezkerreko geziari sakatu behar zaio-----------------------------
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == TECLA_RIGHT)
        {
            irudiakMarraztu();
            pantailaBerriztu();
            egoera = JOLASTEN;
        }
    } while (egoera == AZTERTZEN);
    //Irabazi duzunean egoera aldatzen da eta egoera berria funtzio nagusiari pasatzen zaio
    if (aurkituta == 3)
    {
        egoera = IRABAZI;

    }
    return egoera;
}
//Kartak ezberdinak direnean erabiliko duguna-----------------------------------------------------------------------------------------------------------
void gaizki(int l)
{
    EGOERA egoera = JOLASTEN;
    int ebentu = 0, k = 0;
    m = 0;
    Subtitulo();
    textuaIdatzizuriz(145, 330, "BI HAUEK EZ DIRA BERDINAK..");
    textuaIdatzizuriz(300, 377, "SAIATU BERRIRO!");
    Gaizkisum();
    pantailaBerriztu();
    textuaDesgaitu();
    //Kartak zerrenda batean sartu behin agertu direla ikusteko
    behin[p] = listar[x].random;
    k = zenbatAldiz(behin, 17, behin[p]);
    if (k == 2)
    {
        errepikatuak[z] = listar[x].random;
        z++;
    }
    p++;
    //Puntuazioa negatiboa ez izateko
    if (puntuak <= 0) puntuak = 0;
    else puntuak = puntuak - 3;
    egoera = AZTERTZEN;
    //Kasu honetan ezkerreko geziak kartak berriro ezkutatzen ditu
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == TECLA_RIGHT)
        {

            pantailaGarbitu();
            irudiaKendu(gureGauzak.idKartak);
            gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\fondofuego.bmp");
            irudiakMarraztu();
            kartakmarraztu();
            pantailaBerriztu();
            aurkituta = 0;
            h = 1;
            egoera = JOLASTEN;
            if (x < 5)
            {
                //j++;
                x++;
            }
        }
    } while (egoera == AZTERTZEN);

}
void atzera()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Mapa4);
    irudiaMugitu(Mapa4, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 773 && arratoiarenPos.x < 805 && arratoiarenPos.y> 270 && arratoiarenPos.y < 285)
        {
            sumhasiera();
        }
    } while (egoera == JOLASTEN);
}
void bostgarrenuhartea()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Mapa5);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 420 && arratoiarenPos.x < 520 && arratoiarenPos.y>254 && arratoiarenPos.y < 350)
        {
            aurkezpendesberdintasun();
        }
    } while (egoera == JOLASTEN);
}