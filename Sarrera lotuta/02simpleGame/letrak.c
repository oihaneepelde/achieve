#define _CRT_SECURE_NO_WARNINGS
#include "ourTypes.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include "letrak.h"
#include "zenbakiak.h"
#include "sarrera.h"
#include "koloreak.h"
#include "time.h"

typedef struct S_GURE_GAUZAK
{
    int idArgazkia;

}GURE_GAUZAK;
GURE_GAUZAK gureGauzak;

typedef struct coordenadas
{
    int x;
    int y;
}coordenadas;

coordenadas lista[3];
int non = 0;
char koordenatuak[4];

void hasi()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAPALETRAK);
    irudiaMugitu(MAPALETRAK, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    Sleep(999);
    system("cls");
    menuletrak();
}
void menuletrak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(LETRAKAURKEZPENA);
    irudiaMugitu(LETRAKAURKEZPENA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 180 && arratoiarenPos.x < 808 && arratoiarenPos.y > 240 && arratoiarenPos.y < 411)
        {
            nolajokatuletrak();
        }

    } while (egoera == JOLASTEN);
}
void nolajokatuletrak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(NOLAJOKATU);
    irudiaMugitu(NOLAJOKATU, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 10 && arratoiarenPos.x < 90 && arratoiarenPos.y > 647 && arratoiarenPos.y < 693)
        {
            menuletrak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 860 && arratoiarenPos.x < 935 && arratoiarenPos.y > 650 && arratoiarenPos.y < 684)
        {
            hasijokoa();
        }


    } while (egoera == JOLASTEN);
}
int hasijokoa()
{
    {
        int ebentu = 0;
        EGOERA  egoera = JOLASTEN;
        POSIZIOA arratoiarenPos;
        pantailaGarbitu();
        irudiaKargatu(LETRAKARAUAK);
        irudiaMugitu(LETRAKARAUAK, 0, 0);
        irudiakMarraztu();
        pantailaBerriztu();
        do
        {
            ebentu = ebentuaJasoGertatuBada();
            arratoiarenPos = saguarenPosizioa();
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 8 && arratoiarenPos.x < 85 && arratoiarenPos.y > 645 && arratoiarenPos.y < 691)
            {
                nolajokatuletrak();
            }
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 767 && arratoiarenPos.x < 921 && arratoiarenPos.y > 235 && arratoiarenPos.y < 325)
            {
                hasietxea();
            }


        } while (egoera == JOLASTEN);
    }
}

int hasietxea()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array1[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASIETXEA);
    irudiaMugitu(HASIETXEA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();

    srand((unsigned int)time(NULL));
    non = rand() % 4;
    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array1[non] = 'E';
            printf("%c ", array1[non]);
            sprintf(str, "%C", array1[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));

            if (array0 != 'E')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array1[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array1[i] = array0;
                    printf("%c ", array1[i]);
                    sprintf(str, "%c ", array1[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }

        }
    }
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }

        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 13 && arratoiarenPos.x < 87 && arratoiarenPos.y > 641 && arratoiarenPos.y < 691)
        {
            nolajokatuletrak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 97 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasietxea();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia2();
            textuaIdatzibeltzez(640, 173, "E");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hasiizarra();
        }

    } while (egoera == JOLASTEN);
}

int hasiizarra()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array2[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASIIZARRA);
    irudiaMugitu(HASIIZARRA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();

    srand(time(NULL));
    non = rand() % 4;

    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array2[non] = 'I';
            printf("  %c ", array2[non]);
            sprintf(str, "%C", array2[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));
            if (array0 != 'I')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array2[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array2[i] = array0;
                    printf("%c ", array2[i]);
                    sprintf(str, "%c ", array2[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }
        }
    }

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia2();
            textuaIdatzibeltzez(393, 163, "I");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hasimarrubi();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 97 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasiizarra();
                }

            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}

int hasimarrubi()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array3[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASIMARRUBI);
    irudiaMugitu(HASIMARRUBI, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();

    srand((unsigned int)time(NULL));
    non = rand() % 4;
    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array3[non] = 'A';
            printf("  %c ", array3[non]);
            sprintf(str, "%C", array3[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));
            if (array0 != 'A')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array3[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array3[i] = array0;
                    printf("%c ", array3[i]);
                    sprintf(str, "%c ", array3[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }
        }
    }

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia2();
            textuaIdatzibeltzez(458, 149, "A");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hasitomate();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 97 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasimarrubi();
                }
            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}

int hasitomate()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array4[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASITOMATE);
    irudiaMugitu(HASITOMATE, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();

    srand((unsigned int)time(NULL));
    non = rand() % 4;
    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array4[non] = 'T';
            printf("  %c ", array4[non]);
            sprintf(str, "%C", array4[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));
            if (array0 != 'T')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array4[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array4[i] = array0;
                    printf("%c ", array4[i]);
                    sprintf(str, "%c ", array4[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }
        }
    }

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia2();
            textuaIdatzibeltzez(425, 160, "T");
            textuaIdatzibeltzez(730, 160, "T");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hasiordularia();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 97 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasitomate();
                }
            } while (egoera == JOLASTEN);

        }
    } while (egoera == JOLASTEN);
}

int hasiordularia()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array5[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASIORDULARIA);
    irudiaMugitu(HASIORDULARIA, 0, 0);
    irudiakMarraztu();

    srand((unsigned int)time(NULL));
    non = rand() % 4;
    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array5[non] = 'L';
            printf("  %c ", array5[non]);
            sprintf(str, "%C", array5[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));
            if (array0 != 'L')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array5[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array5[i] = array0;
                    printf("%c ", array5[i]);
                    sprintf(str, "%c ", array5[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }
        }
    }

    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia3();
            textuaIdatzibeltzez(645, 200, "L");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hasizerua();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 97 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasiordularia();
                }

            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}

int hasizerua()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array6[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASIZERUA);
    irudiaMugitu(HASIZERUA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();

    srand((unsigned int)time(NULL));
    non = rand() % 4;
    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array6[non] = 'U';
            printf("  %c ", array6[non]);
            sprintf(str, "%C", array6[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));
            if (array0 != 'U')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array6[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array6[i] = array0;
                    printf("%c ", array6[i]);
                    sprintf(str, "%c ", array6[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }
        }
    }

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia2();
            textuaIdatzibeltzez(685, 178, "U");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hasitxupatxus();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 97 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasizerua();
                }

            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}

int hasitxupatxus()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array7[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASITXUPATXUS);
    irudiaMugitu(HASITXUPATXUS, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();

    srand((unsigned int)time(NULL));
    non = rand() % 4;
    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array7[non] = 'U';
            printf("  %c ", array7[non]);
            sprintf(str, "%C", array7[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));
            if (array0 != 'U')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array7[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array7[i] = array0;
                    printf("%c ", array7[i]);
                    sprintf(str, "%c ", array7[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }
        }
    }

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia3();
            textuaIdatzibeltzez(487, 197, "U");
            textuaIdatzibeltzez(747, 197, "U");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hasitxakurra();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();

            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 87 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasitxupatxus();
                }

            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}

int hasitxakurra()
{
    int ebentu = 0, i = 0, array0 = 0, x = 0, y = 0;
    char str[128], array8[3];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HASITXAKURRA);
    irudiaMugitu(HASITXAKURRA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();

    srand((unsigned int)time(NULL));
    non = rand() % 4;
    for (i = 0; i < 4; i++)
    {
        if (i == non)
        {
            array8[non] = 'K';
            printf("  %c ", array8[non]);
            sprintf(str, "%C", array8[non]);
            textuOsohandia();
            textuaIdatzibeltzez(x + 150, y + 375, str);
            pantailaBerriztu();
            x += 175;
        }
        else
        {
            array0 = 'A' + (rand() % ('Z' - 'A'));
            if (array0 != 'K')
            {
                int j = 0, L = 0;

                for (j = 0; j < i; j++)
                {
                    if (array8[j] == array0)
                    {
                        L = 1;
                    }
                }
                if (L == 1)
                {
                    i--;
                }
                else
                {
                    array8[i] = array0;
                    printf("%c ", array8[i]);
                    sprintf(str, "%c ", array8[i]);
                    textuOsohandia();
                    textuaIdatzibeltzez(x + 150, y + 375, str);
                    pantailaBerriztu();
                    x += 175;
                }
            }
            else { i--; }
        }
    }

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (arratoiarenPos.x > 159 && arratoiarenPos.x < 265 && arratoiarenPos.y > 393 && arratoiarenPos.y < 528)
        {
            koordenatuak[0] = 1;
        }
        else { koordenatuak[0] = 0; }
        if (arratoiarenPos.x > 675 && arratoiarenPos.x < 839 && arratoiarenPos.y > 387 && arratoiarenPos.y < 528)
        {
            koordenatuak[3] = 1;
        }
        else { koordenatuak[3] = 0; }
        if (arratoiarenPos.x > 335 && arratoiarenPos.x < 428 && arratoiarenPos.y > 391 && arratoiarenPos.y < 528)
        {
            koordenatuak[1] = 1;
        }
        else { koordenatuak[1] = 0; }
        if (arratoiarenPos.x > 505 && arratoiarenPos.x < 639 && arratoiarenPos.y > 392 && arratoiarenPos.y < 528)
        {
            koordenatuak[2] = 1;
        }
        else { koordenatuak[2] = 0; }
        if (ebentu == SAGU_BOTOIA_EZKERRA && koordenatuak[non] == 1)
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiOndo);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Ondo();
            textuOsohandia2();
            textuaIdatzibeltzez(612, 170, "K");
            pantailaBerriztu();
            Sleep(999);
            system("cls");
            hitzakeginda();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && ((non != 0 && koordenatuak[0] == 1) || (non != 1 && koordenatuak[1] == 1) || (non != 2 && koordenatuak[2] == 1) || (non != 3 && koordenatuak[3] == 1)))
        {
            gureGauzak.idArgazkia = irudiaKargatu(ArgazkiGaizki);
            irudiaMugitu(gureGauzak.idArgazkia, 0, 354);
            irudiakMarraztu();
            Gaizki();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 97 && arratoiarenPos.x < 848 && arratoiarenPos.y > 121 && arratoiarenPos.y < 565)
                {
                    hasitxakurra();
                }
            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}

int hitzakeginda()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(DESERTUA);
    irudiaMugitu(DESERTUA, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    textuaIdatzibeltzez(350, 75, "OSO ONDO!!");
    textuHandia();
    textuaIdatzibeltzez(200, 200, "Hurrengo jokora pasatzea lortu duzu!!");
    textuaIdatzibeltzez(150, 350, "Joko hau pasatzen baduzu hurrengo uhartera");
    textuaIdatzibeltzez(360, 390, "pasatuko zara.");
    pantailaBerriztu();

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 10 && arratoiarenPos.x < 940 && arratoiarenPos.y > 10 && arratoiarenPos.y < 690)
        {
            hasisopadeletras();
        }
    } while (egoera == JOLASTEN);
}

int hasisopadeletras()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HIZKIZOPAAURKEZPENA);
    irudiaMugitu(HIZKIZOPAAURKEZPENA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 184 && arratoiarenPos.x < 740 && arratoiarenPos.y > 191 && arratoiarenPos.y < 355)
        {
            Arauak();
        }
    } while (egoera == JOLASTEN);
}
int Arauak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(HIZKIZOPAARAUAK);
    irudiaMugitu(HIZKIZOPAARAUAK, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 7 && arratoiarenPos.x < 84 && arratoiarenPos.y > 651 && arratoiarenPos.y < 688)
        {
            hasisopadeletras();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 793 && arratoiarenPos.x < 940 && arratoiarenPos.y > 250 && arratoiarenPos.y < 339)
        {
            sopadeletras();
        }
    } while (egoera == JOLASTEN);
}
int sopadeletras()
{
    int ebentu = 0, i, x = 0, y = 0, j = 0, kont1 = 0, kont2 = 0, k = 0, A = 0, B = 0, D = 0;
    int  K1 = 0, A1 = 0, I = 0, X1 = 0, O = 0, K2 = 0, A2 = 0, X2 = 0, A3 = 0, A4 = 0, H1 = 0, O2 = 0;
    char str[128], array1, array2, array3, array4, array5, ilara1[6], ilara2[6], ilara3[6], ilara4[6], ilara5[6];

    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(DESERTUA);
    irudiaMugitu(DESERTUA, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    textuaIdatzibeltzez(300, 50, "AURKITU HITZAK");

    zuzenaMarraztu(215, 167, 215, 667);//Bertikal
    zuzenaMarraztu(315, 167, 315, 667);//Bertikal
    zuzenaMarraztu(415, 167, 415, 667);//Bertikal
    zuzenaMarraztu(515, 167, 515, 667);//Bertikal
    zuzenaMarraztu(615, 167, 615, 667);//Bertikal
    zuzenaMarraztu(715, 167, 715, 667);//Bertikal

    zuzenaMarraztu(215, 167, 715, 167);//Horizontal
    zuzenaMarraztu(215, 267, 715, 267);//Horizontal
    zuzenaMarraztu(215, 367, 715, 367);//Horizontal
    zuzenaMarraztu(215, 467, 715, 467);//Horizontal
    zuzenaMarraztu(215, 567, 715, 567);//Horizontal
    zuzenaMarraztu(215, 667, 715, 667);//Horizontal

    zuzenaMarraztu(750, 235, 767, 235);//KAIXO
    zuzenaMarraztu(774, 235, 793, 235);//KAIXO
    zuzenaMarraztu(798, 235, 812, 235);//KAIXO
    zuzenaMarraztu(816, 235, 833, 235);//KAIXO
    zuzenaMarraztu(838, 235, 858, 235);//KAIXO

    zuzenaMarraztu(750, 335, 767, 335);//KAXA
    zuzenaMarraztu(774, 335, 793, 335);//KAXA
    zuzenaMarraztu(798, 335, 817, 335);//KAXA
    zuzenaMarraztu(823, 335, 845, 335);//KAXA

    zuzenaMarraztu(750, 435, 767, 435);//AHO
    zuzenaMarraztu(774, 435, 793, 435);//AHO
    zuzenaMarraztu(798, 435, 820, 435);//AHO

    textuHandia();
    textuaIdatzibeltzez(75, 210, "KAIXO");
    textuaIdatzibeltzez(75, 310, "KAXA");
    textuaIdatzibeltzez(75, 410, "AHO");

    pantailaBerriztu();

    srand((unsigned int)time(NULL));

    for (i = 0; i < 5; i++)
    {
        if (i == 0)
        {
            array1 = 'K'; //Lehenengo array-aren 0 posizioan 'K' letra kokatu KAIXO osatzeko
            printf("%c", array1);
            sprintf(str, "%C", array1);
        }
        else
        {
            array1 = 'A' + (rand() % ('Z' - 'A'));  //Hizkiak random eran jarri
            printf("%c ", array1);
            sprintf(str, "%c ", array1);
        }

        textuaIdatzibeltzez(x + 250, y + 200, str);
        x += 100;
        ilara1[j] = array1;
        j++;
    }
    pantailaBerriztu();
    x = 0;
    y = 0;
    for (i = 0; i < 5; i++)
    {
        if (i != 1 && i != 2 && i != 3)
        {
            array2 = 'A' + (rand() % ('Z' - 'A')); //KAIXO eta AHO hitzak osatezen duten posizioak ez direnak random eran jarri
            printf("%c ", array2);
            sprintf(str, "%c ", array2);
        }
        else
        {
            if (i == 1)
            {
                array2 = 'A'; //Bigarren array-aren 1 posizioan 'A' hizkia jarri KAIXO eta AHO osatzeko
                printf("%c", array2);
                sprintf(str, "%c", array2);
            }
            if (i == 2)
            {
                array2 = 'H'; //Bigarren array-aren 1 posizioan 'H' hizkia jarri AHO osatzeko
                printf("%c", array2);
                sprintf(str, "%c", array2);
            }
            if (i == 3)
            {
                array2 = 'O'; //Bigarren array-aren 1 posizioan 'O' hizkia jarri AHO osatzeko
                printf("%c", array2);
                sprintf(str, "%c", array2);
            }

        }
        textuaIdatzibeltzez(x + 250, y + 300, str);
        x += 100;
        ilara2[k] = array2;
        k++;
    }
    pantailaBerriztu();
    x = 0;
    y = 0;
    for (i = 0; i < 5; i++)
    {
        if (i == 2)
        {
            array3 = 'I'; //Hirugarren array-aren 2 posizioan 'I' hizkia jarri KAIXO osatzeko
            printf("%c", array3);
            sprintf(str, "%c", array3);
        }
        else
        {
            array3 = 'A' + (rand() % ('Z' - 'A'));  //Hirugarren array-an hizkiak random eran jarri
            printf("%c", array3);
            sprintf(str, "%c", array3);
        }
        textuaIdatzibeltzez(x + 250, y + 400, str);
        x += 100;
        ilara3[A] = array3;
        A++;
    }
    pantailaBerriztu();
    x = 0;
    y = 0;
    for (i = 0; i < 5; i++)
    {
        if (i == 3)
        {
            array4 = 'X'; //Laugarren array-ko laugarren posizioan 'X' hizkia jarri KAIXO  eta KAXA hitzak osatzeko
            printf("%c", array4);
            sprintf(str, "%c", array4);
        }
        if (i == 4 || i == 2)
        {
            array4 = 'A';  // Laugarren array-ko 3 eta 5 posizioetan 'A' hizkia jarri KAXA hitza osatzeko
            printf("%c", array4);
            sprintf(str, "%c", array4);
        }
        if (i == 1)
        {
            array4 = 'K'; // Laugarren array-ko 2 posizioan 'k' hizkia jarri KAXA hitza osatzeko
            printf("%c", array4);
            sprintf(str, "%c", array4);
        }
        if (i == 0)
        {
            array4 = 'A' + (rand() % ('Z' - 'A')); // Laugarren array-ko 1 posizioan hizkia random eran jarri
            printf("%c", array4);
            sprintf(str, "%c", array4);
        }

        textuaIdatzibeltzez(x + 250, y + 500, str);
        x += 100;
        ilara4[B] = array4;
        B++;
    }
    x = 0;
    y = 0;
    for (i = 0; i < 5; i++)
    {
        if (i == 4)
        {
            array5 = 'O';
            printf("%c", array5);
            sprintf(str, "%c", array5);

        }
        else
        {
            array5 = 'A' + (rand() & ('Z' - 'A'));
            printf("%c", array5);
            sprintf(str, "%c", array5);

        }
        textuaIdatzibeltzez(x + 250, y + 600, str);
        x += 100;
        ilara5[D] = array5;
        D++;
    }

    pantailaBerriztu();
    do
    {
        x = 0;
        y = 0;
        i = 0;
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        struct coordenadas kolisioak = { arratoiarenPos.x, arratoiarenPos.y };
        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            lista[i] = kolisioak;
            if (kont1 < 5)
            {
                if (kolisioak.x > 215 && kolisioak.x < 315 && kolisioak.y > 167 && kolisioak.y < 265)
                {
                    zuzenaMarraztu(215, 167, 315, 267);
                    textuNormala();
                    sprintf(str, "%c", ilara1[0]);
                    textuaIdatzibeltzez(x + 750, y + 210, str);
                    pantailaBerriztu();
                    if (K1 == 0)
                    {
                        kont1++; //Lehenengo hitza osatu arte ezin egin hurrengo hitza
                        K1++; //Baten baino gehiagotan zakatu arren kontadoreak ez egin aurrera
                    }

                }
                if (kolisioak.x > 315 && kolisioak.x < 415 && kolisioak.y > 267 && kolisioak.y < 365)
                {
                    if (A1 == 0 && K1 == 1)
                    {
                        zuzenaMarraztu(315, 267, 415, 367);
                        textuNormala();
                        sprintf(str, "%c", ilara2[1]);
                        textuaIdatzibeltzez(775, 210, str);
                        pantailaBerriztu();
                        kont1++;
                        A1++;
                    }


                }
                if (kolisioak.x > 415 && kolisioak.x < 515 && kolisioak.y > 367 && kolisioak.y < 465)
                {

                    if (I == 0 && A1 == 1)
                    {
                        zuzenaMarraztu(415, 367, 515, 467);
                        textuNormala();
                        sprintf(str, "%c", ilara3[2]);
                        textuaIdatzibeltzez(800, 210, str);
                        pantailaBerriztu();
                        kont1++;
                        I++;
                    }
                }
                if (kolisioak.x > 515 && kolisioak.x < 615 && kolisioak.y > 467 && kolisioak.y < 565)
                {

                    if (X1 == 0 && I == 1)
                    {
                        zuzenaMarraztu(515, 467, 615, 567);
                        textuNormala();
                        sprintf(str, "%c", ilara4[3]);
                        textuaIdatzibeltzez(815, 210, str);
                        pantailaBerriztu();
                        kont1++;
                        X1++;
                    }
                }
                if (kolisioak.x > 615 && kolisioak.x < 715 && kolisioak.y > 567 && kolisioak.y < 665)
                {

                    if (O == 0 && X1 == 1)
                    {
                        zuzenaMarraztu(615, 567, 715, 667);
                        textuNormala();
                        sprintf(str, "%c", ilara5[4]);
                        textuaIdatzibeltzez(840, 210, str);
                        pantailaBerriztu();
                        kont1++;
                        O++;
                        Ondo();
                    }
                }
            }//KAIXO hitza
            if (kont1 >= 5 && kont1 < 9)
            {
                if (kolisioak.x > 315 && kolisioak.x < 415 && kolisioak.y > 470 && kolisioak.y < 570)
                {
                    zuzenaMarraztu(315, 520, 415, 520);
                    textuNormala();
                    sprintf(str, "%c", ilara4[1]);
                    textuaIdatzibeltzez(750, 310, str);
                    pantailaBerriztu();
                    if (K2 == 0)
                    {
                        kont1++;
                        K2++;
                    }
                }
                if (kolisioak.x > 415 && kolisioak.x < 515 && kolisioak.y > 470 && kolisioak.y < 570)
                {

                    if (A2 == 0 && K2 == 1)
                    {
                        zuzenaMarraztu(415, 520, 515, 520);
                        textuNormala();
                        sprintf(str, "%c", ilara4[2]);
                        textuaIdatzibeltzez(775, 310, str);
                        pantailaBerriztu();
                        kont1++;
                        A2++;
                    }
                }
                if (kolisioak.x > 515 && kolisioak.x < 615 && kolisioak.y > 470 && kolisioak.y < 570)
                {

                    if (X2 == 0 && A2 == 1)
                    {
                        zuzenaMarraztu(515, 520, 615, 520);
                        textuNormala();
                        sprintf(str, "%c", ilara4[3]);
                        textuaIdatzibeltzez(800, 310, str);
                        pantailaBerriztu();
                        kont1++;
                        X2++;
                    }
                }
                if (kolisioak.x > 616 && kolisioak.x < 715 && kolisioak.y > 470 && kolisioak.y < 570)
                {

                    if (A3 == 0 && X2 == 1)
                    {
                        zuzenaMarraztu(615, 520, 715, 520);
                        textuNormala();
                        sprintf(str, "%c", ilara4[4]);
                        textuaIdatzibeltzez(825, 310, str);
                        pantailaBerriztu();
                        kont1++;
                        A3++;
                        Ondo();
                    }
                }

            }//KAXA hitza
            if (kont1 >= 9 && kont1 < 12)
            {

                if (kolisioak.x > 315 && kolisioak.x < 415 && kolisioak.y > 270 && kolisioak.y < 370)
                {
                    zuzenaMarraztu(315, 320, 415, 320);
                    textuNormala();
                    sprintf(str, "%c", ilara2[1]);
                    textuaIdatzibeltzez(750, 410, str);
                    pantailaBerriztu();
                    if (A4 == 0)
                    {
                        kont1++;
                        A4++;
                    }
                }
                if (kolisioak.x > 415 && kolisioak.x < 515 && kolisioak.y > 270 && kolisioak.y < 370)
                {
                    if (H1 == 0 && A4 == 1)
                    {

                        zuzenaMarraztu(415, 320, 515, 320);
                        textuNormala();
                        sprintf(str, "%c", ilara2[2]);
                        textuaIdatzibeltzez(775, 410, str);
                        pantailaBerriztu();
                        kont1++;
                        H1++;
                    }
                }
                if (kolisioak.x > 515 && kolisioak.x < 615 && kolisioak.y > 270 && kolisioak.y < 370)
                {

                    if (O2 == 0 && H1 == 1)
                    {
                        zuzenaMarraztu(515, 320, 615, 320);
                        textuNormala();
                        sprintf(str, "%c", ilara2[3]);
                        textuaIdatzibeltzez(800, 410, str);
                        pantailaBerriztu();
                        kont1++;
                        O2++;
                        Ondo();
                    }
                }
            }//AHO hitza
            if (kont1 == 12)
            {
                if (kolisioak.x > 10 && kolisioak.x < 940 && kolisioak.y > 10 && kolisioak.y < 650)
                {
                    Bukaera();
                }

            }//Hurrengo horrira
        }

    } while (egoera == JOLASTEN);
}


int Bukaera()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(DESERTUA);
    irudiaMugitu(DESERTUA, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    textuaIdatzibeltzez(400, 75, "ZORIONAK!!");
    textuHandia();
    textuaIdatzibeltzez(200, 200, "Hurrengo uhartera pasatzea lortu duzu!!");
    textuaIdatzibeltzez(190, 300, "Horrela jarraituz gero altxorra lortuko duzu.");
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 10 && arratoiarenPos.x < 940 && arratoiarenPos.y > 10 && arratoiarenPos.y < 690)
        {
            bigarrenuhartea();
        }

    } while (egoera == JOLASTEN);
}

int bigarrenuhartea()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAPAZENBAKIAK);
    irudiaMugitu(MAPAZENBAKIAK, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 68 && arratoiarenPos.x < 343 && arratoiarenPos.y > 203 && arratoiarenPos.y < 413)
        {
            zenbakiak();
        }
    } while (egoera == JOLASTEN);
}