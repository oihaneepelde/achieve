#ifndef SARRERA
#define SARRERA

#define PORTADA ".\\img\\cerebrordenador.bmp"
#define JOKOA_ADINA ".\\img\\Sarrera ofi2.bmp"
#define MENUHAURRA ".\\img\\menuhaurrak.bmp"
#define MENUGAZTE ".\\img\\Hasiera gazteak2.bmp"
#define MENUHELDU ".\\img\\MenuHelduena.bmp"
#define MAPA ".\\img\\mapa1.bmp"
#define AZALPENHAUR ".\\img\\azalpenakhaurrak2.bmp"
#define KREDITUHAUR ".\\img\\kredituakhaurrak.bmp"
#define JOKATUGAZTE ".\\img\\Nola jokatu.bmp"
#define KREDITUGAZTE ".\\img\\kredituakgazteak.bmp"
#define AZALPENHELDU ".\\img\\NOLAJOKATU.bmp"
#define	KREDITUHELDU ".\\img\\KREDITUAK.bmp"
#define	MAPAHASI ".\\img\\mapa1.bmp"
#define MUSIKA_FONDO ".\\sound\\videoplayback.wav"
#define MAPAAZ ".\\img\\mapaa.bmp"
int jokoaAurkeztu(void);
void aukeratu();
void menuumeak();
void menugazteak();
void menuhelduak();
void hasiu();
void mapahasiera1();
void hasig();
void hasih();
void azalpenakumeak();
void kredituakumeak();
void azalpenakgazteak();
void kredituakgazteak();
void azalpenakhelduak();
void kredituakhelduak();
void Ondo();
void Gaizki();
void musikafondo();

#endif