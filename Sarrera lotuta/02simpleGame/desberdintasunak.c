#define _CRT_SECURE_NO_WARNINGS
#include "ourTypes.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "sarrera.h"
#include "arretaetamemoria.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include <desberdintasunak.h>



#define ONGI_ETORRI_MEZUA "ACHIEVE"
#define JOKOA_SOUND ".\\sound\\132TRANCE_02.wav"
#define JOKOA_PLAYER_IMAGE ".\\img\\invader.bmp"
#define JOKOA_ADINA ".\\img\\Sarrera ofi2.bmp"
#define JOKOA_SOUND_WIN ".\\sound\\BugleCall.wav"
#define JOKOA_SOUND_LOOSE ".\\sound\\terminator.wav" 
#define BUKAERA_SOUND_1 ".\\sound\\128NIGHT_01.wav"
#define BUKAERA_IMAGE ".\\img\\gameOver_1.bmp"

typedef struct S_GURE_GAUZAK
{
    int idIrudi, idMenu, idMapa, idAzalpenak, idKredituak, IdTextua;

}GURE_GAUZAK;
GURE_GAUZAK gureGauzak;


void aurkezpendesberdintasun()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(AURKEZPENDESBER);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 762 && arratoiarenPos.x < 915 && arratoiarenPos.y>597 && arratoiarenPos.y < 662)
        {
            azalpenadesberdin();// azalpenen pantaila
        }
    } while (egoera == JOLASTEN);
}
void azalpenadesberdin()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(AZALPENDESBERDIN);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 776 && arratoiarenPos.x < 885 && arratoiarenPos.y>605 && arratoiarenPos.y < 667)
        {
            aurkezpendesberdintasun();//Jokoko hasierako pantailara joateko
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 776 && arratoiarenPos.x < 885 && arratoiarenPos.y>524 && arratoiarenPos.y < 590)
        {
            mailadesberdintasun(); //Jokoak dituen zailtasun-mailak aukeratzeko pantaila
        }
    } while (egoera == JOLASTEN);
}
void mailadesberdintasun()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAILADESBER);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 910 && arratoiarenPos.y>610 && arratoiarenPos.y < 680)
        {
            azalpenadesberdin(); //Azalpeneko orrira joateko
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 145 && arratoiarenPos.x < 245 && arratoiarenPos.y>410 && arratoiarenPos.y < 477)
        {
            errazadesber(); //Zailtasun-maila errazeko pantailara joateko
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 465 && arratoiarenPos.x < 548 && arratoiarenPos.y>438 && arratoiarenPos.y < 488)
        {
            erdikoadesber();//Zailtasun-maila erdiko pantailara joateko
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 772 && arratoiarenPos.x < 845 && arratoiarenPos.y>445 && arratoiarenPos.y < 500)
        {
            zailadesber();//Zailtasun-maila zaila pantailara joateko
        }
    } while (egoera == JOLASTEN);
}

void errazadesber()
{
    int ebentu = 0, i = 0, klikkop = 0, puntuazioa = 0, D1 = 0, D2 = 0, D3 = 0, D4 = 0;
    char str[128], str2[128];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(SANDIAK);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            if (arratoiarenPos.x > 555 && arratoiarenPos.x < 587 && arratoiarenPos.y>275 && arratoiarenPos.y < 307)
            {
                D1 = 1;
                irudiakMarraztu();
                pantailaBerriztu();
            }
            if (arratoiarenPos.x > 630 && arratoiarenPos.x < 665 && arratoiarenPos.y>275 && arratoiarenPos.y < 307)
            {
                D2 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            if (arratoiarenPos.x > 705 && arratoiarenPos.x < 795 && arratoiarenPos.y>200 && arratoiarenPos.y < 255)
            {
                D3 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            if (arratoiarenPos.x > 710 && arratoiarenPos.x < 795 && arratoiarenPos.y> 405 && arratoiarenPos.y < 485)
            {
                D4 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            else
            {
                irudiakMarraztu();
                pantailaBerriztu();
            }
            puntuazioa = D1 + D2 + D3 + D4;
            klikkop++;
            sprintf(str, "%d", puntuazioa);
            sprintf(str2, "%d", klikkop);
            textuNormala();
            textuaIdatzizuriz(500, 650, str);
            textuaIdatzizuriz(50, 650, "Aurkitutako desberdintasunak:");
            textuaIdatzizuriz(800, 650, str2);
            textuaIdatzizuriz(650, 650, "Saiakerak:");
            i++;
            arkatzKoloreaEzarri(0, 0, 0xFF);
            zirkuluaMarraztu(arratoiarenPos.x, arratoiarenPos.y, 10);
            pantailaBerriztu();
            Sleep(500);
            system("cls");
        }
        if ((klikkop == 10 && puntuazioa == 4 && D1 == 1 && D2 == 1 && D3 == 1 && D4 == 1) || (puntuazioa == 4 && D1 == 1 && D2 == 1 && D3 == 1 && D4 == 1))
        {
            Ondo();//Soinua entzuteko
            pantailaGarbitu();
            irudiaKargatu(SANDIAKOK);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 25 && arratoiarenPos.x < 205 && arratoiarenPos.y>625 && arratoiarenPos.y < 681)
                {
                    errazadesber(); //berriz jokatu
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 755 && arratoiarenPos.x < 925 && arratoiarenPos.y>601 && arratoiarenPos.y < 676)
                {
                    mailadesberdintasun(); //Zailtasun-maila aukeratzeko
                }
            } while (egoera == JOLASTEN);
        }
        if (klikkop > 9 || puntuazioa > 4)
        {
            Gaizki();//Soinua entzuteko
            pantailaGarbitu();
            irudiaKargatu(SANDIAKGAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 25 && arratoiarenPos.x < 205 && arratoiarenPos.y>625 && arratoiarenPos.y < 681)
                {
                    errazadesber();//berriz jokatzeko
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 755 && arratoiarenPos.x < 925 && arratoiarenPos.y>601 && arratoiarenPos.y < 676)
                {
                    mailadesberdintasun();//Zailtasun-maila aukeratzeko
                }
            } while (egoera == JOLASTEN);
        }

    } while (egoera == JOLASTEN);
}
void erdikoadesber()
{
    int ebentu = 0, i = 0, klikkop = 0, puntuazioa = 0, D1 = 0, D2 = 0, D3 = 0, D4 = 0;
    char str[128], str2[128];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(BOLAK);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            if (arratoiarenPos.x > 622 && arratoiarenPos.x < 660 && arratoiarenPos.y>192 && arratoiarenPos.y < 202)
            {
                D1 = 1;
                irudiakMarraztu();
                pantailaBerriztu();
            }
            if (arratoiarenPos.x > 498 && arratoiarenPos.x < 594 && arratoiarenPos.y>308 && arratoiarenPos.y < 400)
            {
                D2 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            if (arratoiarenPos.x > 721 && arratoiarenPos.x < 785 && arratoiarenPos.y>338 && arratoiarenPos.y < 403)
            {
                D3 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            if (arratoiarenPos.x > 530 && arratoiarenPos.x < 610 && arratoiarenPos.y> 496 && arratoiarenPos.y < 548)
            {
                D4 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            else
            {
                irudiakMarraztu();
                pantailaBerriztu();
            }
            puntuazioa = D1 + D2 + D3 + D4;
            klikkop++;
            sprintf(str, "%d", puntuazioa);
            sprintf(str2, "%d", klikkop);
            textuNormala();
            textuaIdatzizuriz(500, 650, str);
            textuaIdatzizuriz(50, 650, "Aurkitutako desberdintasunak:");
            textuaIdatzizuriz(800, 650, str2);
            textuaIdatzizuriz(650, 650, "Saiakerak:");
            i++;
            arkatzKoloreaEzarri(0, 0, 0xFF);
            zirkuluaMarraztu(arratoiarenPos.x, arratoiarenPos.y, 10);
            pantailaBerriztu();
            Sleep(500);
            system("cls");
        }
        if ((klikkop == 10 && puntuazioa == 4 && D1 == 1 && D2 == 1 && D3 == 1 && D4 == 1) || (puntuazioa == 4 && D1 == 1 && D2 == 1 && D3 == 1 && D4 == 1))
        {
            Ondo();//Soinua entzuteko
            pantailaGarbitu();
            irudiaKargatu(BOLAKOK);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 32 && arratoiarenPos.x < 185 && arratoiarenPos.y>615 && arratoiarenPos.y < 680)
                {
                    erdikoadesber();//berriz jokatu
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 765 && arratoiarenPos.x < 925 && arratoiarenPos.y>601 && arratoiarenPos.y < 684)
                {
                    mailadesberdintasun();//Zailtasun-maila aukeratu
                }
            } while (egoera == JOLASTEN);
        }
        if (klikkop > 9 || puntuazioa > 4)
        {
            Gaizki();//Soinua entzuteko
            pantailaGarbitu();
            irudiaKargatu(BOLAKGAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 32 && arratoiarenPos.x < 185 && arratoiarenPos.y>615 && arratoiarenPos.y < 680)
                {
                    erdikoadesber();//Berriz jokatu
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 765 && arratoiarenPos.x < 925 && arratoiarenPos.y>601 && arratoiarenPos.y < 684)
                {
                    mailadesberdintasun();// Zailtasun-maila aukeratu
                }
            } while (egoera == JOLASTEN);
        }

    } while (egoera == JOLASTEN);
}
void zailadesber()
{
    int ebentu = 0, i = 0, klikkop = 0, puntuazioa = 0, D1 = 0, D2 = 0, D3 = 0, D4 = 0, D5 = 0;
    char str[128], str2[128];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ELEFANTEA);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            if (arratoiarenPos.x > 590 && arratoiarenPos.x < 620 && arratoiarenPos.y>262 && arratoiarenPos.y < 285)
            {
                D1 = 1;
                irudiakMarraztu();
                pantailaBerriztu();
            }
            if (arratoiarenPos.x > 680 && arratoiarenPos.x < 775 && arratoiarenPos.y>247 && arratoiarenPos.y < 340)
            {
                D2 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            if (arratoiarenPos.x > 746 && arratoiarenPos.x < 770 && arratoiarenPos.y>425 && arratoiarenPos.y < 458)
            {
                D3 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            if (arratoiarenPos.x > 638 && arratoiarenPos.x < 662 && arratoiarenPos.y> 425 && arratoiarenPos.y < 458)
            {
                D4 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            if (arratoiarenPos.x > 800 && arratoiarenPos.x < 830 && arratoiarenPos.y> 475 && arratoiarenPos.y < 507)
            {
                D5 = 1;
                irudiakMarraztu();
                pantailaBerriztu();

            }
            else
            {
                irudiakMarraztu();
                pantailaBerriztu();
            }
            puntuazioa = D1 + D2 + D3 + D4 + D5;
            klikkop++;
            sprintf(str, "%d", puntuazioa);
            sprintf(str2, "%d", klikkop);
            textuNormala();
            textuaIdatzizuriz(500, 650, str);
            textuaIdatzizuriz(50, 650, "Aurkitutako desberdintasunak:");
            textuaIdatzizuriz(800, 650, str2);
            textuaIdatzizuriz(650, 650, "Saiakerak:");
            i++;
            arkatzKoloreaEzarri(0, 0, 0xFF);
            zirkuluaMarraztu(arratoiarenPos.x, arratoiarenPos.y, 10);
            pantailaBerriztu();
            Sleep(500);
            system("cls");
        }
        if ((klikkop == 10 && puntuazioa == 5 && D1 == 1 && D2 == 1 && D3 == 1 && D4 == 1 && D5 == 1) || (puntuazioa == 5 && D1 == 1 && D2 == 1 && D3 == 1 && D4 == 1 && D5 == 1))
        {
            Ondo();//Soinua entzuteko
            pantailaGarbitu();
            irudiaKargatu(ELEFANTEAOK);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 24 && arratoiarenPos.x < 180 && arratoiarenPos.y>609 && arratoiarenPos.y < 680)
                {
                    zailadesber();//berriz jokatu
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 755 && arratoiarenPos.x < 915 && arratoiarenPos.y>601 && arratoiarenPos.y < 677)
                {
                    mailadesberdintasun();//Zailtasun-maila aukeratu
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 550 && arratoiarenPos.x < 700 && arratoiarenPos.y>605 && arratoiarenPos.y < 675)
                {
                    seigarrenuhartea();
                }
            } while (egoera == JOLASTEN);
        }
        if (klikkop > 9 || puntuazioa > 5)
        {
            Gaizki();//Soinua entzuteko
            pantailaGarbitu();
            irudiaKargatu(ELEFANTEAGAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 24 && arratoiarenPos.x < 180 && arratoiarenPos.y>609 && arratoiarenPos.y < 680)
                {
                    zailadesber();//berriz jokatu
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 755 && arratoiarenPos.x < 915 && arratoiarenPos.y>601 && arratoiarenPos.y < 677)
                {
                    mailadesberdintasun();//Zailtasun-maila aukeratu
                }
            } while (egoera == JOLASTEN);
        }

    } while (egoera == JOLASTEN);
}
void seigarrenuhartea()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAPADA);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 557 && arratoiarenPos.x < 840 && arratoiarenPos.y>347 && arratoiarenPos.y < 612)
        {
            arretauhartea();
        }
    } while (egoera == JOLASTEN);
}