#define _CRT_SECURE_NO_WARNINGS
#include "ourTypes.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "sarrera.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include <arretaetamemoria.h>
void arretauhartea()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETA);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 495 && arratoiarenPos.x < 840 && arratoiarenPos.y>170 && arratoiarenPos.y < 540)
        {
            azalpenaarreta();//Azalpenetako pantaila joateko
        }
    } while (egoera == JOLASTEN);
}
void hasiarreta()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAHASI);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 795 && arratoiarenPos.y>212 && arratoiarenPos.y < 574)
        {
            arretagalderak();//Galderak erantzuteko pantailara joateko
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 825 && arratoiarenPos.x < 935 && arratoiarenPos.y>615 && arratoiarenPos.y < 680)
        {
            azalpenaarreta();//Azalpenen pantailara itzultzeko
        }
    } while (egoera == JOLASTEN);
}
void arretagalderak()
{
    int ebentu = 0, A1 = 0, A2 = 0, A3 = 0, A4 = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAGALDERAK);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>245 && arratoiarenPos.y < 275)
        {
            A1 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>340 && arratoiarenPos.y < 370)
        {
            A2 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 182 && arratoiarenPos.y>435 && arratoiarenPos.y < 466)
        {
            A3 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 145 && arratoiarenPos.x < 180 && arratoiarenPos.y>525 && arratoiarenPos.y < 555)
        {
            A4 = 1;
        }
        if (A4 == 1 && A3 == 0 && A2 == 0 && A1 == 0) //Erantzuna asmatuz gero
        {
            Ondo();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            pantailaGarbitu();
            irudiaKargatu(GALDERAKONDO);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 698 && arratoiarenPos.x < 845 && arratoiarenPos.y>525 && arratoiarenPos.y < 590)
                {
                    irudiaKargatu(ARRETAHASIP);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    do
                    {
                        ebentu = ebentuaJasoGertatuBada();
                        arratoiarenPos = saguarenPosizioa();
                        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 767 && arratoiarenPos.x < 895 && arratoiarenPos.y>616 && arratoiarenPos.y < 681)
                        {
                            arretahasi2();//Hurrengo orrira joateko
                        }
                    } while (egoera == JOLASTEN);
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 767 && arratoiarenPos.x < 895 && arratoiarenPos.y>616 && arratoiarenPos.y < 681)
                {
                    arretahasi2();
                }
            } while (egoera == JOLASTEN);

        }
        if (A4 == 0 && (A3 == 1 || A2 == 1 || A1 == 1)) //Erantzuna gaizki eginez gero
        {
            Gaizki();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            irudiaKargatu(GALDERAKGAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            Sleep(350);
            system("cls");
            arretagalderak();
        }
    } while (egoera == JOLASTEN);
}
void arretahasi2()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAHASI2);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 75 && arratoiarenPos.x < 865 && arratoiarenPos.y>100 && arratoiarenPos.y < 610)
        {
            arretagalderak2(); //Galdera erantzuteko orrira joateko
        }
    } while (egoera == JOLASTEN);
}
void arretagalderak2()
{
    int ebentu = 0, A1 = 0, A2 = 0, A3 = 0, A4 = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAGALDERAK2);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>245 && arratoiarenPos.y < 275)
        {
            A1 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>340 && arratoiarenPos.y < 370)
        {
            A2 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 182 && arratoiarenPos.y>435 && arratoiarenPos.y < 466)
        {
            A3 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 145 && arratoiarenPos.x < 180 && arratoiarenPos.y>525 && arratoiarenPos.y < 555)
        {
            A4 = 1;
        }
        if (A1 == 1 && A3 == 0 && A2 == 0 && A4 == 0)//Galdera asmatzean
        {
            Ondo();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            pantailaGarbitu();
            irudiaKargatu(ARRETAGALDERAK2ONDO);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 698 && arratoiarenPos.x < 845 && arratoiarenPos.y>525 && arratoiarenPos.y < 590)
                {
                    //Irudia ikusteko
                    irudiaKargatu(ARRETAHASI2KOP);
                    irudiakMarraztu();
                    pantailaBerriztu();
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 775 && arratoiarenPos.x < 916 && arratoiarenPos.y>609 && arratoiarenPos.y < 845)
                {
                    arretahasi3(); //Hurrengo argazkia

                }
            } while (egoera == JOLASTEN);
        }
        if (A1 == 0 && (A3 == 1 || A2 == 1 || A4 == 1))//Galdera ez asmatu bitartean
        {
            Gaizki();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            irudiaKargatu(ARRETAGALDERAK2GAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            Sleep(350);
            system("cls");
            arretagalderak2();//berriz hasteko zailtasun-maila
        }
    } while (egoera == JOLASTEN);
}
void arretahasi3()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAHASI3);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 105 && arratoiarenPos.x < 825 && arratoiarenPos.y>150 && arratoiarenPos.y < 560)
        {
            arretagalderak3(); //Galdera erantzuteko orrira joateko
        }
    } while (egoera == JOLASTEN);
}
void arretagalderak3()
{
    int ebentu = 0, A1 = 0, A2 = 0, A3 = 0, A4 = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAGALDERAK3);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>245 && arratoiarenPos.y < 275)
        {
            A1 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>340 && arratoiarenPos.y < 370)
        {
            A2 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 182 && arratoiarenPos.y>435 && arratoiarenPos.y < 466)
        {
            A3 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 145 && arratoiarenPos.x < 180 && arratoiarenPos.y>525 && arratoiarenPos.y < 555)
        {
            A4 = 1;
        }
        if (A1 == 0 && A3 == 0 && A2 == 0 && A4 == 1)//Galdera asmatzean
        {
            Ondo();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            pantailaGarbitu();
            irudiaKargatu(ARRETAGALDERAK3ONDO);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 698 && arratoiarenPos.x < 845 && arratoiarenPos.y>525 && arratoiarenPos.y < 590)
                {
                    //Irudia ikusteko
                    irudiaKargatu(ARRETAHASI3KOP);
                    irudiakMarraztu();
                    pantailaBerriztu();
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 808 && arratoiarenPos.x < 930 && arratoiarenPos.y>615 && arratoiarenPos.y < 685)
                {
                    arretahasi4(); //Hurrengo argazkia

                }
            } while (egoera == JOLASTEN);
        }
        if (A4 == 0 && (A3 == 1 || A2 == 1 || A1 == 1))//Galdera ez asmatu bitartean
        {
            Gaizki();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            irudiaKargatu(ARRETAGALDERAK3GAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            Sleep(350);
            system("cls");
            arretagalderak3();//berriz hasteko zailtasun-maila
        }
    } while (egoera == JOLASTEN);
}
void arretahasi4()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAHASI4);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 135 && arratoiarenPos.x < 810 && arratoiarenPos.y>173 && arratoiarenPos.y < 555)
        {
            arretagalderak4(); //Galdera erantzuteko orrira joateko
        }
    } while (egoera == JOLASTEN);
}
void arretagalderak4()
{
    int ebentu = 0, A1 = 0, A2 = 0, A3 = 0, A4 = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAGALDERAK4);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>245 && arratoiarenPos.y < 275)
        {
            A1 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>340 && arratoiarenPos.y < 370)
        {
            A2 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 182 && arratoiarenPos.y>435 && arratoiarenPos.y < 466)
        {
            A3 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 145 && arratoiarenPos.x < 180 && arratoiarenPos.y>525 && arratoiarenPos.y < 555)
        {
            A4 = 1;
        }
        if (A1 == 0 && A3 == 1 && A2 == 0 && A4 == 0)//Galdera asmatzean
        {
            Ondo();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            pantailaGarbitu();
            irudiaKargatu(ARRETAGALDERAK4ONDO);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 711 && arratoiarenPos.x < 840 && arratoiarenPos.y>495 && arratoiarenPos.y < 587)
                {
                    //Irudia ikusteko
                    irudiaKargatu(ARRETAHASI4KOP);
                    irudiakMarraztu();
                    pantailaBerriztu();
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 811 && arratoiarenPos.x < 918 && arratoiarenPos.y>615 && arratoiarenPos.y < 683)
                {

                    arretahasi5(); //Hurrengo argazkia

                }
            } while (egoera == JOLASTEN);
        }
        if (A3 == 0 && (A4 == 1 || A2 == 1 || A1 == 1))//Galdera ez asmatu bitartean
        {
            Gaizki();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            irudiaKargatu(ARRETAGALDERAK4GAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            Sleep(350);
            system("cls");
            arretagalderak4();//berriz hasteko zailtasun-maila
        }
    } while (egoera == JOLASTEN);
}
void arretahasi5()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAHASI5);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 135 && arratoiarenPos.x < 810 && arratoiarenPos.y>173 && arratoiarenPos.y < 555)
        {
            arretagalderak5(); //Galdera erantzuteko orrira joateko
        }
    } while (egoera == JOLASTEN);
}
void arretagalderak5()
{
    int ebentu = 0, A1 = 0, A2 = 0, A3 = 0, A4 = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAGALDERAK5);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>245 && arratoiarenPos.y < 275)
        {
            A1 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 185 && arratoiarenPos.y>340 && arratoiarenPos.y < 370)
        {
            A2 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 150 && arratoiarenPos.x < 182 && arratoiarenPos.y>435 && arratoiarenPos.y < 466)
        {
            A3 = 1;
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 145 && arratoiarenPos.x < 180 && arratoiarenPos.y>525 && arratoiarenPos.y < 555)
        {
            A4 = 1;
        }
        if (A1 == 0 && A3 == 1 && A2 == 0 && A4 == 0)//Galdera asmatzean
        {
            Ondo();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            pantailaGarbitu();
            irudiaKargatu(ARRETAGALDERAK5ONDO);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 697 && arratoiarenPos.x < 850 && arratoiarenPos.y>511 && arratoiarenPos.y < 591)
                {
                    //Irudia ikusteko
                    irudiaKargatu(ARRETAHASI5KOP);
                    irudiakMarraztu();
                    pantailaBerriztu();
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 788 && arratoiarenPos.x < 924 && arratoiarenPos.y>618 && arratoiarenPos.y < 673)
                {

                    BUKATU(); //Altxorraren bila jokoa amaitzeko eta altxorra lortzeko

                }
            } while (egoera == JOLASTEN);
        }
        if (A3 == 0 && (A4 == 1 || A2 == 1 || A1 == 1))//Galdera ez asmatu bitartean
        {
            Gaizki();//Soinua entzuteko
            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;
            irudiaKargatu(ARRETAGALDERAK5GAIZKI);
            irudiakMarraztu();
            pantailaBerriztu();
            Sleep(350);
            system("cls");
            arretagalderak5();//berriz hasteko zailtasun-maila
        }
    } while (egoera == JOLASTEN);
}
void BUKATU()
{
    int ebentu = 0, A1 = 0, A2 = 0, A3 = 0, A4 = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    //irudiaKendu(gureGauzak.idIrudi);
    irudiaKargatu(MAPAALTXOR);
    //irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 778 && arratoiarenPos.x < 878 && arratoiarenPos.y>4 && arratoiarenPos.y < 60)
        {
            irudiaKargatu(ALTXORRA);
            irudiakMarraztu();
            pantailaBerriztu();
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 751 && arratoiarenPos.x < 930 && arratoiarenPos.y>578 && arratoiarenPos.y < 667)
                {
                    jokoaAurkeztu();
                }
            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}
void azalpenaarreta()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ARRETAAZALPEN);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 638 && arratoiarenPos.x < 761 && arratoiarenPos.y>593 && arratoiarenPos.y < 665)
        {
            arretauhartea(); // arreta eta memoria lantzeko jokoko menura joateko
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 638 && arratoiarenPos.x < 761 && arratoiarenPos.y>521 && arratoiarenPos.y < 583)
        {
            hasiarreta(); //jokoa hasteko
        }
    } while (egoera == JOLASTEN);
}