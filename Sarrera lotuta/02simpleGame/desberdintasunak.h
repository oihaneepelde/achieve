#ifndef DESBERDINTASUNAK_H
#define DESBERDINTASUNAK_H
#define IMAGEN_H
#define MAX_IMG 200
#define AURKEZPENDESBER ".\\img\\desberdintasunenbilaaurkezpen.bmp"
#define AZALPENDESBERDIN ".\\img\\desberdinazalpen.bmp"
#define MAILADESBER ".\\img\\mailadesber.bmp"
#define SANDIAK ".\\img\\sandiabukatuta.bmp"
#define SANDIAKGAIZKI ".\\img\\sandiakbukatu2.0.bmp"
#define SANDIAKOK ".\\img\\sandiakbukatu1.0.bmp"
#define BOLAK ".\\img\\bolak.bmp"
#define BOLAKGAIZKI ".\\img\\bolakbukatu2.0.bmp"
#define BOLAKOK ".\\img\\bolakbukatu1.0.bmp"
#define ELEFANTEA ".\\img\\elefantea.bmp"
#define ELEFANTEAGAIZKI ".\\img\\elefanteabukatu2.0.bmp"
#define ELEFANTEAOK ".\\img\\elefanteabukatu1.0.bmp"

//MAPEN ARGAZKIAK
//#define MAPA ".\\img\\mapaa.bmp"
#define MAPA45 ".\\img\\mapa4-5.bmp"
#define MAPADA ".\\img\\mapa5-6.bmp"

//SOINUAK
#define GAIZKI ".\\sound\\Gaizki.wav"
#define ONDO ".\\sound\\Ondo.wav"

//DESBERDINTASUNAK AURKITZEKO JOKOKO FUNTZIOAK
void aurkezpendesberdintasun();
void azalpenadesberdin();
void mailadesberdintasun();
void errazadesber();
void erdikoadesber();
void zailadesber();

//HURRENGO JOKORA JOATEKO
void seigarrenuhartea();

#endif