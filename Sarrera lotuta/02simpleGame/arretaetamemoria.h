#ifndef ARRETAETAMEMORIA_H
#define ARRETAETAMEMORIA_H
#define IMAGEN_H
#define MAX_IMG 200


//ARRETA ETA MEMORIA JOKOKO ARGAZKIAK
#define ARRETA ".\\img\\arreta.bmp"
#define ARRETAHASI ".\\img\\arretahasi.bmp"
#define ARRETAGALDERAK ".\\img\\arretagalderak.bmp"
#define GALDERAKGAIZKI ".\\img\\galderakgaizki.bmp"
#define GALDERAKONDO ".\\img\\galderakondo.bmp"
#define ARRETAHASIP ".\\img\\arretahasip.bmp"
#define ARRETAHASI2 ".\\img\\arretahasi2.bmp"
#define ARRETAGALDERAK2 ".\\img\\arretagalderak2.bmp"
#define ARRETAGALDERAK2GAIZKI ".\\img\\arretagaldera2gaizki.bmp"
#define ARRETAGALDERAK2ONDO ".\\img\\arretagalderak2ondo.bmp"
#define ARRETAHASI2KOP ".\\img\\arretahasi2KOP.bmp"
#define ARRETAAZALPEN ".\\img\\arretaazalpen.bmp"
#define ARRETAHASI3 ".\\img\\arretahasi3.bmp"
#define ARRETAGALDERAK3 ".\\img\\arretagalderak3.bmp"
#define ARRETAGALDERAK3GAIZKI ".\\img\\arretagalderak3gaizki.bmp"
#define ARRETAGALDERAK3ONDO ".\\img\\arretagalderak3ondo.bmp"
#define ARRETAHASI3KOP ".\\img\\arretahasi3KOP.bmp"
#define ARRETAHASI4 ".\\img\\arretahasi4.bmp"
#define ARRETAGALDERAK4 ".\\img\\arretagalderak4.bmp"
#define ARRETAGALDERAK4GAIZKI ".\\img\\arretagalderak4gaizki.bmp"
#define ARRETAGALDERAK4ONDO ".\\img\\arretagalderak4ondo.bmp"
#define ARRETAHASI4KOP ".\\img\\arretahasi4KOP.bmp"
#define ARRETAHASI5 ".\\img\\arretahasi5.bmp"
#define ARRETAGALDERAK5 ".\\img\\arretagalderak5.bmp"
#define ARRETAGALDERAK5GAIZKI ".\\img\\arretagalderak5gaizki.bmp"
#define ARRETAGALDERAK5ONDO ".\\img\\arretagalderak5ondo.bmp"
#define ARRETAHASI5KOP ".\\img\\arretahasi5KOP.bmp"

//ALTXORRAREN IRLAKO ARGAZKIAK
#define MAPAALTXOR ".\\img\\MAPAALTXOR.bmp"
#define ALTXORRA ".\\img\\altxorrapantaila.bmp"


//MAPEN ARGAZKIAK
#define MAPADA ".\\img\\mapa5-6.bmp"


//SOINUAK
#define GAIZKI ".\\sound\\Gaizki.wav"
#define ONDO ".\\sound\\Ondo.wav"


//ARRETA ETA MEMORIA JOKOKO FUNTZIOAK
void arretauhartea();
void azalpenaarreta();
void hasiarreta();
void arretagalderak();
void arretahasi2();
void arretagalderak2();
void arretahasi3();
void arretagalderak3();
void arretahasi4();
void arretagalderak4();
void arretahasi5();
void arretagalderak5();

//ALTXORRAREN FUNTZIOAK
void BUKATU();

#endif