#define _CRT_SECURE_NO_WARNINGS
#include "sarrera.h"
#include "ourTypes.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include "letrak.h"
/*#include "zenbakiak.h"
#include "koloreak.h"*/
void musikafondo()
{
    Mix_Music* musika = NULL;
    audioInit();
    loadTheMusic(MUSIKA_FONDO);
    playMusic();
}
int jokoaAurkeztu(void)
{
    EGOERA  egoera = JOLASTEN;
    int ebentu = 0;
    //char str[128];

    POSIZIOA arratoiarenPos;

    pantailaGarbitu();
    irudiaKargatu(PORTADA);
    irudiaMugitu(PORTADA, 0, 0);
    irudiakMarraztu();
    musikafondo();

    pantailaBerriztu();

    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 236 && arratoiarenPos.x < 761 && arratoiarenPos.y>269 && arratoiarenPos.y < 413)
        {
            aukeratu();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 808 && arratoiarenPos.x < 888 && arratoiarenPos.y>575 && arratoiarenPos.y < 654)
        {
            sgItxi();
            return 0;
        }
    } while (egoera == JOLASTEN);

}
void aukeratu()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(JOKOA_ADINA);
    irudiaMugitu(JOKOA_ADINA, 0, 0);
    irudiakMarraztu();

    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 72 && arratoiarenPos.x < 320 && arratoiarenPos.y>372 && arratoiarenPos.y < 587)
        {
            menuumeak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 360 && arratoiarenPos.x < 622 && arratoiarenPos.y>372 && arratoiarenPos.y < 587)
        {
            menugazteak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 627 && arratoiarenPos.x < 881 && arratoiarenPos.y>372 && arratoiarenPos.y < 587)
        {
            menuhelduak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 881 && arratoiarenPos.x < 931 && arratoiarenPos.y>596 && arratoiarenPos.y < 678)
        {
            jokoaAurkeztu();
        }
    } while (egoera == JOLASTEN);
}

void menuumeak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MENUHAURRA);
    irudiaMugitu(MENUHAURRA, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 193 && arratoiarenPos.x < 334 && arratoiarenPos.y>227 && arratoiarenPos.y < 364)
        {
            hasiu();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 492 && arratoiarenPos.x < 643 && arratoiarenPos.y>304 && arratoiarenPos.y < 444)
        {
            azalpenakumeak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 447 && arratoiarenPos.x < 579 && arratoiarenPos.y>145 && arratoiarenPos.y < 268)
        {
            kredituakumeak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 833 && arratoiarenPos.x < 933 && arratoiarenPos.y>609 && arratoiarenPos.y < 690)
        {
            aukeratu();
        }
    } while (egoera == JOLASTEN);
}
void menugazteak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MENUGAZTE);
    irudiaMugitu(MENUGAZTE, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 546 && arratoiarenPos.y>157 && arratoiarenPos.y < 287)
        {
            hasig();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 766 && arratoiarenPos.x < 890 && arratoiarenPos.y>311 && arratoiarenPos.y < 434)
        {
            azalpenakgazteak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 67 && arratoiarenPos.x < 198 && arratoiarenPos.y>306 && arratoiarenPos.y < 427)
        {
            kredituakgazteak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 820 && arratoiarenPos.x < 928 && arratoiarenPos.y>608 && arratoiarenPos.y < 682)
        {
            aukeratu();
        }
    } while (egoera == JOLASTEN);
}
void menuhelduak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MENUHELDU);
    irudiaMugitu(MENUHELDU, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 370 && arratoiarenPos.x < 562 && arratoiarenPos.y>176 && arratoiarenPos.y < 299)
        {
            hasih();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 674 && arratoiarenPos.x < 892 && arratoiarenPos.y>396 && arratoiarenPos.y < 556)
        {
            azalpenakhelduak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 48 && arratoiarenPos.x < 238 && arratoiarenPos.y>394 && arratoiarenPos.y < 527)
        {
            kredituakhelduak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 845 && arratoiarenPos.x < 941 && arratoiarenPos.y>623 && arratoiarenPos.y < 695)
        {
            aukeratu();
        }
    } while (egoera == JOLASTEN);
}
void hasiu()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAPAAZ);
    //irudiaKargatu(HASIERAKOMAPA);
    //irudiaMugitu(HASIERAKOMAPA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 829 && arratoiarenPos.x < 933 && arratoiarenPos.y>584 && arratoiarenPos.y < 668)
        {
            mapahasiera1();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 230 && arratoiarenPos.x < 350 && arratoiarenPos.y>390 && arratoiarenPos.y < 500)
        {
            hasi();
        }
    } while (egoera == JOLASTEN);
}
void mapahasiera1()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAPAHASI);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 230 && arratoiarenPos.x < 350 && arratoiarenPos.y>390 && arratoiarenPos.y < 500)
        {
            hasi();
        }
    } while (egoera == JOLASTEN);
}
void hasig()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAPA);
    irudiaMugitu(MAPA, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 840 && arratoiarenPos.x < 938 && arratoiarenPos.y>594 && arratoiarenPos.y < 663)
        {
            menugazteak();
        }
    } while (egoera == JOLASTEN);;
}
void hasih()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(MAPA);
    irudiaMugitu(MAPA, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 840 && arratoiarenPos.x < 938 && arratoiarenPos.y>594 && arratoiarenPos.y < 663)
        {
            menuhelduak();
        }
    } while (egoera == JOLASTEN);
}
void azalpenakumeak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(AZALPENHAUR);
    irudiaMugitu(AZALPENHAUR, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 722 && arratoiarenPos.x < 821 && arratoiarenPos.y>594 && arratoiarenPos.y < 663)
        {
            menuumeak();
        }
    } while (egoera == JOLASTEN);
}
void kredituakumeak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(KREDITUHAUR);
    irudiaMugitu(KREDITUHAUR, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 736 && arratoiarenPos.x < 816 && arratoiarenPos.y>586 && arratoiarenPos.y < 646)
        {
            menuumeak();
        }
    } while (egoera == JOLASTEN);
}
void azalpenakgazteak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(JOKATUGAZTE);
    irudiaMugitu(JOKATUGAZTE, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 792 && arratoiarenPos.x < 929 && arratoiarenPos.y>586 && arratoiarenPos.y < 672)
        {
            menugazteak();
        }
    } while (egoera == JOLASTEN);
}
void kredituakgazteak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(KREDITUGAZTE);
    irudiaMugitu(KREDITUGAZTE, 0, 0);
    irudiakMarraztu();


    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 812 && arratoiarenPos.x < 925 && arratoiarenPos.y>608 && arratoiarenPos.y < 681)
        {
            menugazteak();
        }
    } while (egoera == JOLASTEN);
}
void azalpenakhelduak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(AZALPENHELDU);
    irudiaMugitu(AZALPENHELDU, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 812 && arratoiarenPos.x < 925 && arratoiarenPos.y>608 && arratoiarenPos.y < 681)
        {
            menuhelduak();
        }
    } while (egoera == JOLASTEN);
}
void Ondo() //Soinua ondo egiten duenean
{
    int idSound;
    Mix_Music* musika = NULL;

    audioInit();
    idSound = loadSound(ONDO);
    playSound(idSound);

}
void Gaizki() //soinuea gaizki egiten duenean
{
    int idSound;
    Mix_Music* musika = NULL;

    audioInit();
    idSound = loadSound(GAIZKI);
    playSound(idSound);

}
void kredituakhelduak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(KREDITUHELDU);
    irudiaMugitu(KREDITUHELDU, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 860 && arratoiarenPos.x < 945 && arratoiarenPos.y>636 && arratoiarenPos.y < 690)
        {
            menuhelduak();
        }
    } while (egoera == JOLASTEN);
}