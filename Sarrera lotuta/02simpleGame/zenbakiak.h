#ifndef ZENBAKIAK
#define ZENBAKIAK

#define ZNBKHASIERA ".\\img\\znbkhasiera.bmp"
#define ZNBKSARRERA ".\\img\\znbkazalpenaa.bmp"
#define ZNBKAZALPENA1 ".\\img\\znbkkartakazalpena.bmp"
#define ZNBKAZALPENA2 ".\\img\\znbkpuntuakazalpena.bmp"
#define BOSQUE ".\\img\\bosque.bmp"
#define ZENBAKIA1 ".\\img\\1.bmp"
#define ZENBAKIA2 ".\\img\\2.bmp"
#define ZENBAKIA3 ".\\img\\3.bmp"
#define ZENBAKIA4 ".\\img\\4.bmp"
#define ZENBAKIA5 ".\\img\\5.bmp"
#define ZENBAKIA6 ".\\img\\6.bmp"
#define ZENBAKIA7 ".\\img\\7.bmp"
#define ZENBAKIA8 ".\\img\\8.bmp"
#define ZENBAKIA9 ".\\img\\9.bmp"
#define OBJ1 ".\\img\\leon1.bmp"
#define OBJ2 ".\\img\\obj2.bmp"
#define OBJ3 ".\\img\\obj3.bmp"
#define OBJ4 ".\\img\\obj4.bmp"
#define OBJ5 ".\\img\\obj5.bmp"
#define OBJ6 ".\\img\\obj6.bmp"
#define OBJ7 ".\\img\\obj7.bmp"
#define OBJ8 ".\\img\\obj8.bmp"
#define OBJ9 ".\\img\\obj9.bmp"
#define PUNTUAK1 ".\\img\\puntuak1.bmp"
#define PUNTUAK2 ".\\img\\caracol.bmp"
#define PUNTUAK3 ".\\img\\frog.bmp"
#define PUNTUAK4 ".\\img\\oilarra.bmp"
#define ESCAPE ".\\img\\ESC.bmp"
#define OH ".\\img\\saiatuberriz.bmp"
#define ZORIONAK ".\\img\\zorionak.bmp"
#define ONDO ".\\sound\\Ondo.wav"
#define GAIZKI ".\\sound\\Gaizki.wav"
#define Mapa3 ".\\img\\Mapa nire 2.bmp"

//int musika();
void zenbakiak();
void znbkkokatu();
void znbkmugitu();
void puntuenpantaila(kont);
void marraztu();
void hirugarrenuhartea();
void puntuakzuzendu();
#endif