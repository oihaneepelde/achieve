#define _CRT_SECURE_NO_WARNINGS
#include "ourTypes.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include "zenbakiak.h"
#include "sarrera.h"
#include "koloreak.h"
#include "letrak.h"
#include "time.h"

//aldagai globalak
int errezenb[3];
int erreobj[3];
int puntuazioa = 0;
int ezkerkartapos = 0;
int x = 0;
int i = 0;
int buelta = 0;
int idSound;

//karten irudiak
int zenbaki[] = { ZENBAKIA1, ZENBAKIA2, ZENBAKIA3, ZENBAKIA4, ZENBAKIA5, ZENBAKIA6, ZENBAKIA7, ZENBAKIA8, ZENBAKIA9 };
int objetuak[] = { OBJ1, OBJ2, OBJ3, OBJ4, OBJ5, OBJ6, OBJ7, OBJ8, OBJ9 };

//puntuen irudiak
int puntuakargazki[] = { PUNTUAK1, PUNTUAK2, PUNTUAK3, PUNTUAK4 };

typedef struct s_kartak
{
    int id1, id2, id3, obj1, obj2, obj3;
}KARTAK;
KARTAK kartak;
typedef struct s_jokalaria
{
    int x;
    int y;
}JOKALARIA;

JOKALARIA klikpuntu[10];
void zenbakiak()
{

    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(ZNBKHASIERA);
    irudiaMugitu(ZNBKHASIERA, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    //hasi sakatzean sarreratxoa
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 208 && arratoiarenPos.x < 712 && arratoiarenPos.y>268 && arratoiarenPos.y < 410)
        {

            irudiaKargatu(ZNBKSARRERA);
            irudiaMugitu(ZNBKSARRERA, 0, 0);
            irudiakMarraztu();
            pantailaBerriztu();
            //lehen jokuaren azalpenera
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                //bigarren jokuaren azalpena
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 762 && arratoiarenPos.x < 873 && arratoiarenPos.y>593 && arratoiarenPos.y < 645)
                {

                    irudiaKargatu(ZNBKAZALPENA1);
                    irudiaMugitu(ZNBKAZALPENA1, 0, 0);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    do
                    {
                        ebentu = ebentuaJasoGertatuBada();
                        arratoiarenPos = saguarenPosizioa();
                        //lehen jokua hasi
                        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 152 && arratoiarenPos.x < 522 && arratoiarenPos.y>170 && arratoiarenPos.y < 239)
                        {

                            znbkkokatu();
                        }
                    } while (egoera == JOLASTEN);
                }
            } while (egoera == JOLASTEN);
        }
    } while (egoera == JOLASTEN);
}
void znbkkokatu()
{

    while (buelta < 2)
    {
        int i, lehena, bigarrena, hirugarrena, err, erre;
        //argazkien arrayaren tamaina
        int dimzen = sizeof(zenbaki) / sizeof(*zenbaki);

        pantailaGarbitu();
        irudiaKargatu(BOSQUE);
        irudiaMugitu(BOSQUE, 0, 0);
        irudiakMarraztu();
        textuaIdatzizuriz(50, 650, "Puntuazioa:");
        pantailaBerriztu();



        // --------- ezker 3 kartak random

        srand((unsigned int)time(NULL));

        int random = rand() % (dimzen);
        kartak.id1 = irudiaKargatu(zenbaki[random]);
        irudiaMugitu(kartak.id1, 60, 70);
        irudiakMarraztu();

        errezenb[x] = random;



        random = rand() % (dimzen);
        for (i = x; i < x + 2; i++)
        {
            err = errezenb[i];

            while (random == err)
            {

                random = rand() % (dimzen);
            }
        }
        kartak.id2 = irudiaKargatu(zenbaki[random]);
        irudiaMugitu(kartak.id2, 60, 270);
        irudiakMarraztu();

        errezenb[x + 1] = random;


        random = rand() % (dimzen);
        for (i = x; i < x + 2; i++)
        {
            err = errezenb[i];
            erre = errezenb[x];
            while (random == err || random == erre)
            {
                random = rand() % (dimzen);
            }
        }
        kartak.id3 = irudiaKargatu(zenbaki[random]);
        irudiaMugitu(kartak.id3, 60, 470);
        irudiakMarraztu();

        errezenb[x + 2] = random;

        pantailaBerriztu();

        //  ------ aurrez atera diren 3en bikoteak eskuinaldean ateratzeko------

        lehena = errezenb[0];
        bigarrena = errezenb[1];
        hirugarrena = errezenb[2];
        // ----------------------------------------
            //eskuinaldeko kartak random

        random = rand() % (dimzen);
        for (i = x; i < x + 2; i++)
        {
            err = erreobj[i];
            while (random == err || random != lehena && random != bigarrena && random != hirugarrena)
            {
                random = rand() % (dimzen);
            }
        }
        kartak.obj1 = irudiaKargatu(objetuak[random]);
        irudiaMugitu(kartak.obj1, 730, 70);
        irudiakMarraztu();

        erreobj[x] = random;



        random = rand() % (dimzen);
        for (i = x; i < x + 2; i++)
        {
            err = erreobj[i];
            while (random == err || random != lehena && random != bigarrena && random != hirugarrena)
            {
                random = rand() % (dimzen);
            }
        }
        kartak.obj2 = irudiaKargatu(objetuak[random]);
        irudiaMugitu(kartak.obj2, 730, 270);
        irudiakMarraztu();

        erreobj[x + 1] = random;


        random = rand() % (dimzen);
        for (i = x; i < x + 2; i++)
        {
            err = erreobj[i];
            erre = erreobj[x];
            while ((random == err || random == erre) || random != lehena && random != bigarrena && random != hirugarrena)
            {
                random = rand() % (dimzen);
            }
        }
        kartak.obj3 = irudiaKargatu(objetuak[random]);
        irudiaMugitu(kartak.obj3, 730, 470);
        irudiakMarraztu();

        erreobj[x + 2] = random;

        pantailaBerriztu();

        znbkmugitu();
    }
}
void znbkmugitu()
{
    Mix_Music* musika = NULL;
    int ebentu = 0;
    char str[128];
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    int kont = 0;

    while (buelta < 3)
    {

        do
        {
            ebentu = ebentuaJasoGertatuBada();
            arratoiarenPos = saguarenPosizioa();
            //eskuin lehenengoa
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x < 888 && arratoiarenPos.y < 261 && arratoiarenPos.x>728 && arratoiarenPos.y> 71)
            {
                do
                {
                    ebentu = ebentuaJasoGertatuBada();
                    arratoiarenPos = saguarenPosizioa();
                    if (ebentu == SAGU_MUGIMENDUA)
                    {
                        irudiaMugitu(kartak.obj1, arratoiarenPos.x, arratoiarenPos.y);
                        irudiakMarraztu();
                        pantailaBerriztu();
                    }
                } while (ebentu != SAGU_BOTOIA_EZKERRA);
                //ezker kartaren pos
                if (arratoiarenPos.x < 216 && arratoiarenPos.y < 261 && arratoiarenPos.x>58 && arratoiarenPos.y>68)
                {
                    ezkerkartapos = 0;
                    arratoiarenPos.y = 68;
                }
                else if (arratoiarenPos.x < 216 && arratoiarenPos.y < 458 && arratoiarenPos.x>58 && arratoiarenPos.y>269)
                {
                    ezkerkartapos = 1;
                    arratoiarenPos.y = 269;
                }
                else if (arratoiarenPos.x < 216 && arratoiarenPos.y < 658 && arratoiarenPos.x>58 && arratoiarenPos.y>470)
                {
                    ezkerkartapos = 2;
                    arratoiarenPos.y = 470;
                }
                //bikoteak detektatu
                if (erreobj[0] == errezenb[ezkerkartapos])
                {
                    Ondo();
                    irudiaMugitu(kartak.obj1, 270, arratoiarenPos.y);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    kont++;
                    puntuazioa = puntuazioa + 10;
                }
                else
                {
                    Gaizki();
                    irudiaMugitu(kartak.obj1, 728, 71);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    puntuazioa = puntuazioa - 5;
                    if (puntuazioa < 0) puntuazioa = 0;

                }
            }
            //eskuin bigarrena
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x < 888 && arratoiarenPos.y < 458 && arratoiarenPos.x>728 && arratoiarenPos.y> 269)
            {

                do
                {
                    ebentu = ebentuaJasoGertatuBada();
                    arratoiarenPos = saguarenPosizioa();
                    if (ebentu == SAGU_MUGIMENDUA)
                    {
                        irudiaMugitu(kartak.obj2, arratoiarenPos.x, arratoiarenPos.y);
                        irudiakMarraztu();
                        pantailaBerriztu();
                    }
                } while (ebentu != SAGU_BOTOIA_EZKERRA);
                //ezker kartaren pos

                if (arratoiarenPos.x < 216 && arratoiarenPos.y < 261 && arratoiarenPos.x>58 && arratoiarenPos.y>68)
                {
                    ezkerkartapos = 0;
                    arratoiarenPos.y = 68;
                }
                else if (arratoiarenPos.x < 216 && arratoiarenPos.y < 458 && arratoiarenPos.x>58 && arratoiarenPos.y>269)
                {
                    ezkerkartapos = 1;
                    arratoiarenPos.y = 269;
                }
                else if (arratoiarenPos.x < 216 && arratoiarenPos.y < 658 && arratoiarenPos.x>58 && arratoiarenPos.y>470)
                {
                    ezkerkartapos = 2;
                    arratoiarenPos.y = 470;
                }
                //bikoteak detektatu
                if (erreobj[1] == errezenb[ezkerkartapos])
                {
                    Ondo();
                    irudiaMugitu(kartak.obj2, 270, arratoiarenPos.y);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    kont++;
                    puntuazioa = puntuazioa + 10;

                }
                else
                {
                    Gaizki();
                    irudiaMugitu(kartak.obj2, 728, 269);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    puntuazioa = puntuazioa - 5;
                    if (puntuazioa < 0) puntuazioa = 0;
                }
            }

            //eskuin azkena
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x < 888 && arratoiarenPos.y < 661 && arratoiarenPos.x>728 && arratoiarenPos.y> 469)
            {
                do
                {
                    ebentu = ebentuaJasoGertatuBada();
                    arratoiarenPos = saguarenPosizioa();
                    if (ebentu == SAGU_MUGIMENDUA)
                    {
                        irudiaMugitu(kartak.obj3, arratoiarenPos.x, arratoiarenPos.y);
                        irudiakMarraztu();
                        pantailaBerriztu();
                    }
                } while (ebentu != SAGU_BOTOIA_EZKERRA);
                //ezker kartaren pos
                if (arratoiarenPos.x < 216 && arratoiarenPos.y < 261 && arratoiarenPos.x>58 && arratoiarenPos.y>68)
                {
                    ezkerkartapos = 0;
                    arratoiarenPos.y = 68;
                }
                else if (arratoiarenPos.x < 216 && arratoiarenPos.y < 458 && arratoiarenPos.x>58 && arratoiarenPos.y>269)
                {
                    ezkerkartapos = 1;
                    arratoiarenPos.y = 269;
                }
                else if (arratoiarenPos.x < 216 && arratoiarenPos.y < 658 && arratoiarenPos.x>58 && arratoiarenPos.y>470)
                {
                    ezkerkartapos = 2;
                    arratoiarenPos.y = 470;
                }
                //bikoteak detektatu
                if (erreobj[2] == errezenb[ezkerkartapos])
                {
                    Ondo();
                    irudiaMugitu(kartak.obj3, 270, arratoiarenPos.y);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    kont++;
                    puntuazioa = puntuazioa + 10;
                }
                else
                {
                    Gaizki();
                    irudiaMugitu(kartak.obj3, 728, 469);
                    irudiakMarraztu();
                    pantailaBerriztu();
                    puntuazioa = puntuazioa - 5;
                    if (puntuazioa < 0) puntuazioa = 0;
                }
            }
            sprintf(str, "%d", puntuazioa);
            textuNormala();
            textuaIdatzizuriz(750, 20, str);
            textuaIdatzizuriz(600, 20, "Puntuazioa:");
            pantailaBerriztu();
        } while (kont < 3);
        buelta++;
        Sleep(999);
        system("cls");
        znbkkokatu();
    }
    //Sleep(999);
    //system("cls");
    irudiaKargatu(ZNBKAZALPENA2);
    irudiaMugitu(ZNBKAZALPENA2, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 147 && arratoiarenPos.x < 535 && arratoiarenPos.y>176 && arratoiarenPos.y < 246)
        {

            puntuenpantaila();
        }
    } while (egoera == JOLASTEN);
}
void puntuenpantaila(kont)
{

    char str[128];
    while (i < 1)
    {
        irudiaKargatu(puntuakargazki[i]);
        irudiaMugitu(puntuakargazki[i], 0, 0);
        irudiakMarraztu();
        sprintf(str, "%d", puntuazioa);
        textuNormala();
        textuaIdatzizuriz(750, 20, str);
        textuaIdatzizuriz(600, 20, "Puntuazioa:");
        pantailaBerriztu();
        arkatzKoloreaEzarri(233, 19, 164);
        marraztu();
    }
    if (puntuazioa >= 20)
    {
        irudiaKargatu(ZORIONAK);
        irudiaMugitu(ZORIONAK, 0, 0);
        irudiakMarraztu();
        sprintf(str, "%d", puntuazioa);
        textuaGaitu();
        textuaIdatzizuriz(270, 210, str);
        pantailaBerriztu();
        Sleep(1000);
        system("cls");
        hirugarrenuhartea();//hurrengo uhartera
    }
    else
    {
        irudiaKargatu(OH);
        irudiaMugitu(OH, 0, 0);
        irudiakMarraztu();
        sprintf(str, "%d", puntuazioa);
        textuaGaitu();
        textuaIdatzizuriz(270, 210, str);
        pantailaBerriztu();
        Sleep(2000);
        system("cls");
        puntuazioa = 0;
        buelta = 0;
        kont = 0;
        i = 0;
        hasiu();
    }
}
void marraztu()
{
    
    int ebentu = 0, klik = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;

    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        JOKALARIA jokalariaklik = { arratoiarenPos.x, arratoiarenPos.y };

        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            klikpuntu[klik] = jokalariaklik;
            klik++;
            do
            {
                ebentu = ebentuaJasoGertatuBada();
                arratoiarenPos = saguarenPosizioa();
                JOKALARIA jokalariaklik = { arratoiarenPos.x, arratoiarenPos.y };
                if (ebentu == SAGU_MUGIMENDUA)
                {
                    zuzenaMarraztu(arratoiarenPos.x, arratoiarenPos.y, arratoiarenPos.x, arratoiarenPos.y);
                    pantailaBerriztu();
                }
                if (ebentu == SAGU_BOTOIA_EZKERRA)
                {
                    klikpuntu[klik] = jokalariaklik;
                    klik++;
                }
                if (ebentu == TECLA_ESCAPE) puntuenpantaila();
            } while (klik != 10);
        }
    } while (klik != 10);
    puntuakzuzendu();
    Sleep(888);
    system("cls");
    i++;
    puntuenpantaila();
}
void puntuakzuzendu()
{

    int i;
    int puntux[] = { 353, 452, 572, 556, 533, 336, 328, 323, 428, 516 };
    int puntuy[] = { 142, 112, 80, 190, 305, 149, 263, 356, 332, 309 };

    for (i = 0; i < 10; i++)
    {
        if (puntux[i] + 10 > klikpuntu[i].x && puntux[i] < klikpuntu[i].x && puntuy[i] < klikpuntu[i].y && puntuy[i] + 10 > klikpuntu[i].y)
        {
            puntuazioa++;
        }
    }
}
//y(142, 112, 80, 190, 305, 149, 263, 356, 332, 312)
//x(353, 452, 572, 556, 533, 336, 328, 323, 428, 51 )
void hirugarrenuhartea()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Mapa3);
    irudiaMugitu(Mapa3, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 304 && arratoiarenPos.x < 417 && arratoiarenPos.y > 60 && arratoiarenPos.y < 160)
        {
            koloreak();
        }
    } while (egoera == JOLASTEN);
}