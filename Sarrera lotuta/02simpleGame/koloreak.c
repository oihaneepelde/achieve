#define _CRT_SECURE_NO_WARNINGS
#include "ourTypes.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "bikoteak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include "zenbakiak.h"
#include "sarrera.h"
#include "koloreak.h"
#include "letrak.h"

typedef struct S_GURE_GAUZAK
{
    int idArgazkia;

}GURE_GAUZAK;
GURE_GAUZAK gureGauzak;

int puntuak = 0;

int koloreak() //Sarrera
{

    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(SarreraH); //Sarrerako irudia
    irudiaMugitu(SarreraH, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 233 && arratoiarenPos.x < 726 && arratoiarenPos.y>210 && arratoiarenPos.y < 415) // click itean hurrengo pantaila
        {
            Expli();
        }
    } while (egoera == JOLASTEN);
}
void Expli() //Nola jolastu
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Jarraitu); // explikazioaren argazkia
    irudiaMugitu(Jarraitu, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 865 && arratoiarenPos.x < 943 && arratoiarenPos.y> 627 && arratoiarenPos.y < 690) // click itean hurrengo pantaila
        {
            Ikastera();
        }
    } while (egoera == JOLASTEN);
}
void borobilak() //borobilak egin bakoitza  kolore batez
{
    arkatzKoloreaEzarri(0XFF, 0XFF, 0XFF); //txuria
    zirkuluaMarraztu(151, 152, 44);
    arkatzKoloreaEzarri(0, 0, 0);  //beltza
    zirkuluaMarraztu(290, 152, 44);
    arkatzKoloreaEzarri(249, 137, 19); //laranja
    zirkuluaMarraztu(429, 152, 44);
    arkatzKoloreaEzarri(136, 77, 15); //marroia
    zirkuluaMarraztu(565, 152, 44);
    arkatzKoloreaEzarri(247, 236, 81);  // horia
    zirkuluaMarraztu(696, 152, 44);
    arkatzKoloreaEzarri(247, 81, 214);  //roxa
    zirkuluaMarraztu(323, 355, 44);
    arkatzKoloreaEzarri(189, 62, 222); //morea
    zirkuluaMarraztu(446, 355, 44);
    arkatzKoloreaEzarri(255, 0, 0);   //gorria
    zirkuluaMarraztu(573, 355, 44);
    arkatzKoloreaEzarri(0, 0, 255);  //urdina
    zirkuluaMarraztu(703, 355, 44);
    arkatzKoloreaEzarri(0, 255, 0); //berdea
    zirkuluaMarraztu(829, 355, 44);
}


void Ikastera()










// koloreak ikasteko
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi); //fondoa
    irudiaMugitu(Ikasi, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193) //clik egitean borobila handitu
        {
            pantailaGarbitu();

            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(0XFF, 0XFF, 0XFF); //txuria borobila
            zirkuluaMarraztu(495, 340, 160);
            textuaIdatzibeltzez(360, 310, "ZURIA");
            int idSound; // kolorearen audioa
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Zuria);
            playSound(idSound);


            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //buelta atzera
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(0, 0, 0);  //beltza borobila
            zirkuluaMarraztu(495, 340, 160);
            textuaIdatzizuriz(332, 310, "BELTZA");
            int idSound; // kolorearen audioa
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Beltza);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //atzera bueltatu
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)  //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(249, 137, 19); //laranja borobila
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu2();
            textuaIdatzizuriz(332, 310, "LARANJA");
            int idSound; // soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Laranja);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(136, 77, 15); //marroia
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu3();
            textuaIdatzizuriz(342, 310, "MARROIA");
            int idSound; // soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Marroia);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518)  //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(247, 236, 81);  // horia
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu();
            textuaIdatzizuriz(350, 310, "HORIA");
            int idSound; //soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Horia);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(247, 81, 214);  //roxa
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu2();
            textuaIdatzizuriz(350, 310, "ARROSA");
            int idSound; // soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Arrosa);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(189, 62, 222); //morea
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu4();
            textuaIdatzizuriz(350, 310, "MOREA");
            int idSound; // soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Morea);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(0XFF, 0, 0);   //gorria
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu4();
            textuaIdatzizuriz(340, 310, "GORRIA");
            int idSound; //soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Gorria);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(0, 0, 0XFF);  //urdina
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu4();
            textuaIdatzizuriz(335, 310, "URDINA");
            int idSound;  //soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Urdina);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518) //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395) //clik egitean borobila handitu
        {
            pantailaGarbitu();
            irudiaKargatu(Extra);
            irudiaMugitu(Extra, 0, 0);
            irudiakMarraztu();
            arkatzKoloreaEzarri(0, 0XFF, 0); //berdea
            zirkuluaMarraztu(495, 340, 160);
            textuaGaitu4();
            textuaIdatzizuriz(335, 310, "BERDEA");
            int idSound; //soinua kolorea
            Mix_Music* musika = NULL;

            audioInit();
            idSound = loadSound(Berdea);
            playSound(idSound);

            pantailaBerriztu();
            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 311 && arratoiarenPos.x < 384 && arratoiarenPos.y> 470 && arratoiarenPos.y < 518)  //atzera buelta
                {
                    Ikastera();
                }

            } while (egoera == JOLASTEN);
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 793 && arratoiarenPos.x < 931 && arratoiarenPos.y> 598 && arratoiarenPos.y < 691) //Hurrengo jolasaren explikazioa
        {
            explikazioa();
        }

    } while (egoera == JOLASTEN);
}
void explikazioa() //2. jolasaren explikazioa
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Expli1);
    irudiaMugitu(Expli1, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 876 && arratoiarenPos.x < 940 && arratoiarenPos.y> 633 && arratoiarenPos.y < 690) //Click itean jolasten hasi
        {
            Jolastera();
        }
    } while (egoera == JOLASTEN);
}

int Jolastera() //jolasa 1 zatia
{

    int ebentu = 0;//untuak = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi21);
    irudiaMugitu(Ikasi21, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193) // c�ick itean ondo
        {
            irudiaKargatu(Ikasi221);
            irudiaMugitu(Ikasi221, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik); //ondo ikurra
            irudiaMugitu(gureGauzak.idArgazkia, 128, 130);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa); //Hurrengora joateko ikurra
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(120, 200, "Zuria");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) //2 zatia joan
                {
                    Jolastera2();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // beste danetan zakautakun gaizki

        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki2();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki2();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki2()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi221);
    //irudiaMugitu(Ikasi221, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix1); // gaizki ikurra
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(120, 200, "Zuria");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) //2 zatia
        {
            Jolastera2();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera2() // jolasaren 2 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi210);
    irudiaMugitu(Ikasi210, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394) // click itean ondo
        {

            irudiaKargatu(Ikasi222);
            irudiaMugitu(Ikasi222, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik10);
            irudiaMugitu(gureGauzak.idArgazkia, 808, 333);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(793, 403, "Berdea");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) //3 zatia
                {
                    Jolastera3();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // klick egitean gaizki 

        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki3();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)

        {
            Gaizki3();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki3()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi2210);
    //irudiaMugitu(Ikasi2210, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix2);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(793, 403, "Berdea");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 3 zatia
        {
            Jolastera3();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera3() // jolasaren 3 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi22);
    irudiaMugitu(Ikasi22, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194) //click itean ondo

        {
            irudiaKargatu(Ikasi223);
            irudiaMugitu(Ikasi223, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik2);
            irudiaMugitu(gureGauzak.idArgazkia, 268, 130);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(260, 200, "Beltza");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 4 zatia
                {
                    Jolastera4();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki4();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)

        {
            Gaizki4();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki4()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi222);
    //irudiaMugitu(Ikasi222, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix3);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(260, 200, "Beltza");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) //4 zatia

        {
            Jolastera4();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera4() //jolasaren 4 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi29);
    irudiaMugitu(Ikasi29, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395) // click egitan ondo

        {

            irudiaKargatu(Ikasi224);
            irudiaMugitu(Ikasi224, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik9);
            irudiaMugitu(gureGauzak.idArgazkia, 682, 333);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(673, 403, "Urdina");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 5 zatia
                {
                    Jolastera5();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)

        {
            Gaizki5();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki5();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki5()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi229);
    //irudiaMugitu(Ikasi229, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix4);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(673, 403, "Urdina");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 5 zatia
        {
            Jolastera5();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera5() // jolasaren 5 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi23);
    irudiaMugitu(Ikasi23, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194) // click egitean ondo

        {

            irudiaKargatu(Ikasi225);
            irudiaMugitu(Ikasi225, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik3);
            irudiaMugitu(gureGauzak.idArgazkia, 408, 130);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(390, 200, "Laranja");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 6 zatia
                {
                    Jolastera6();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395)
        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki6();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)

        {
            Gaizki6();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki6()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi223);
    //irudiaMugitu(Ikasi223, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix5);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(390, 200, "Laranja");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 6 zatia
        {
            Jolastera6();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera6() // jolasaren 6 zatia 
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi28);
    irudiaMugitu(Ikasi28, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394) // click egitean ondo
        {

            irudiaKargatu(Ikasi226);
            irudiaMugitu(Ikasi226, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik8);
            irudiaMugitu(gureGauzak.idArgazkia, 550, 333);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(540, 403, "Gorria");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) //7 zatia
                {
                    Jolastera7();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395)
        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki7();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)
        {
            Gaizki7();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki7()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi228);
    //irudiaMugitu(Ikasi228, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix6);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(540, 403, "Gorria");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 7 zatia
        {
            Jolastera7();
        }

    } while (egoera == JOLASTEN);
    return puntuak;

}
int Jolastera7() // jolasaren 7 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi24);
    irudiaMugitu(Ikasi24, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194) // click egitean ondo

        {

            irudiaKargatu(Ikasi227);
            irudiaMugitu(Ikasi227, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik4);
            irudiaMugitu(gureGauzak.idArgazkia, 543, 130);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(525, 200, "Marroia");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 8 zatia
                {
                    Jolastera8();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395)
        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki8();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)
        {
            Gaizki8();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki8()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi224);
    //irudiaMugitu(Ikasi224, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix7);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(525, 200, "Marroia");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 8 zatia
        {
            Jolastera8();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera8() // jolasaren 8 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi27);
    irudiaMugitu(Ikasi27, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394) // click egitean ondo


        {

            irudiaKargatu(Ikasi228);
            irudiaMugitu(Ikasi228, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik7);
            irudiaMugitu(gureGauzak.idArgazkia, 425, 333);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(410, 403, "Morea");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) //9 zatia
                {
                    Jolastera9();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } // click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395)
        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki9();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)
        {
            Gaizki9();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki9()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    gureGauzak.idArgazkia = irudiaKargatu(Ekix8);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(410, 403, "Morea");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 9 zatia
        {
            Jolastera9();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera9() // jolasaren 9 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi25);
    irudiaMugitu(Ikasi25, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)	 // click egitean ondo


        {

            irudiaKargatu(Ikasi229);
            irudiaMugitu(Ikasi229, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik5);
            irudiaMugitu(gureGauzak.idArgazkia, 673, 130);
            gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(668, 200, "Horia");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 10 zatia
                {
                    Jolastera10();
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        } //click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395)
        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki10();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)
        {
            Gaizki10();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki10()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi225);
    //irudiaMugitu(Ikasi225, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix9);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Hurrengoa);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(668, 200, "Horia");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // 10 zatia
        {
            Jolastera10();
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
int Jolastera10() // jolasaren 10 zatia
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKargatu(Ikasi26);
    irudiaMugitu(Ikasi26, 0, 0);
    irudiakMarraztu();
    borobilak();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 284 && arratoiarenPos.x < 367 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394) // click egitean ondo
        {

            irudiaKargatu(Ikasi2210);
            irudiaMugitu(Ikasi2210, 0, 0);
            gureGauzak.idArgazkia = irudiaKargatu(Tik6);
            irudiaMugitu(gureGauzak.idArgazkia, 300, 333);
            gureGauzak.idArgazkia = irudiaKargatu(Amaitu);
            irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
            irudiakMarraztu();
            textuNormala();
            textuaIdatzibeltzez(290, 403, "Arrosa");
            puntuak = puntuak + 10;
            Ondo();
            pantailaBerriztu();

            do
            {
                arratoiarenPos = saguarenPosizioa();
                ebentu = ebentuaJasoGertatuBada();
                if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // click egitean puntuaziora
                {
                    Sarrera4(puntuak);
                }

            } while (egoera == JOLASTEN);
            return puntuak;
        }// click egitean gaizki
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 790 && arratoiarenPos.x < 875 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)

        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 664 && arratoiarenPos.x < 750 && arratoiarenPos.y> 313 && arratoiarenPos.y < 395)
        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 534 && arratoiarenPos.x < 619 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 491 && arratoiarenPos.y> 313 && arratoiarenPos.y < 394)
        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 657 && arratoiarenPos.x < 744 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 526 && arratoiarenPos.x < 613 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 390 && arratoiarenPos.x < 476 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 251 && arratoiarenPos.x < 335 && arratoiarenPos.y> 111 && arratoiarenPos.y < 194)
        {
            Gaizki11();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 112 && arratoiarenPos.x < 201 && arratoiarenPos.y> 110 && arratoiarenPos.y < 193)
        {
            Gaizki11();
        }
    } while (egoera == JOLASTEN);

}
int Gaizki11()
{
    int ebentu = 0;
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    //irudiaKargatu(Ikasi226);
    //irudiaMugitu(Ikasi226, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Ekix10);
    irudiaMugitu(gureGauzak.idArgazkia, 0, 0);
    gureGauzak.idArgazkia = irudiaKargatu(Amaitu);
    irudiaMugitu(gureGauzak.idArgazkia, 809, 646);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzibeltzez(290, 403, "Arrosa");
    puntuak = puntuak - 3;
    Gaizki();
    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 809 && arratoiarenPos.x < 947 && arratoiarenPos.y> 646 && arratoiarenPos.y < 680) // click egitean puntuaziora
        {
            Sarrera4(puntuak);
        }

    } while (egoera == JOLASTEN);
    return puntuak;
}
void Sarrera4(int puntuak) // puntuazioa
{
    int ebentu = 0;
    char str[128];
    EGOERA egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    sprintf(str, "%d", puntuak); // puntuak kontatu
    textuaIdatzibeltzez(361, 208, str); // puntuak pantailaratu
    pantailaBerriztu();
    if (puntuak < 50) // 50 baino gutziago bada pantaila honetara sartu
    {
        if (puntuak < 0) puntuak = 0;
        irudiaKargatu(SarreraB1);
        irudiaMugitu(SarreraB1, 0, 0);
        irudiakMarraztu();
        sprintf(str, "%d", puntuak);
        textuaGaitu();
        textuaIdatzibeltzez(299, 230, str);
        textuNormala1();
        textuaIdatzibeltzez(100, 351, " Zure puntuazioa 50 baino ");
        textuNormala1();
        textuaIdatzibeltzez(98, 401, " gutxiago denez, berriro  ");
        textuNormala1();
        textuaIdatzibeltzez(98, 451, " jolastu behar duzu. ");
        pantailaBerriztu();

        do
        {
            arratoiarenPos = saguarenPosizioa();
            ebentu = ebentuaJasoGertatuBada();
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 825 && arratoiarenPos.x < 941 && arratoiarenPos.y> 602 && arratoiarenPos.y < 684) // berriro hasi
            {
                puntuak = 0;
                koloreak();
            }
        } while (egoera == JOLASTEN);

    }
    else // 50 baino gehiago
    {

        irudiaKargatu(SarreraB);
        irudiaMugitu(SarreraB, 0, 0);
        irudiakMarraztu();

        sprintf(str, "%d", puntuak);
        textuaGaitu();
        textuaIdatzibeltzez(299, 230, str);
        textuNormala1();
        textuaIdatzibeltzez(100, 351, " Zure puntuazioa 50 baino ");
        textuNormala1();
        textuaIdatzibeltzez(98, 401, " gehiago denez, irabazi");
        textuNormala1();
        textuaIdatzibeltzez(98, 451, " duzu. ZORIONAK! ");
        pantailaBerriztu();
        do
        {
            arratoiarenPos = saguarenPosizioa();
            ebentu = ebentuaJasoGertatuBada();
            if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 825 && arratoiarenPos.x < 941 && arratoiarenPos.y> 602 && arratoiarenPos.y < 684) // hurrengo jolasera
            {
                int ebentu = 0;
                EGOERA egoera = JOLASTEN;
                POSIZIOA arratoiarenPos;
                pantailaGarbitu();
                irudiaKargatu(Mapa4);
                irudiaMugitu(Mapa4, 0, 0);
                irudiakMarraztu();
                pantailaBerriztu();
                do
                {
                    arratoiarenPos = saguarenPosizioa();
                    ebentu = ebentuaJasoGertatuBada();
                    if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 773 && arratoiarenPos.x < 805 && arratoiarenPos.y> 207 && arratoiarenPos.y < 285)
                    {
                        sumhasiera();
                    }
                } while (egoera == JOLASTEN);

            }
        } while (egoera == JOLASTEN);
    }

}