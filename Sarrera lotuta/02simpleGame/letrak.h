#ifndef LETRAK
#define LETRAK

#define HASIERAKOMAPA ".\\img\\mapa1.bmp"
#define IZENAIDATZI ".\\img\\ZureIzenaFondo.bmp"
#define MAPALETRAK ".\\img\\mapahasi1.bmp"
#define LETRAKAURKEZPENA ".\\img\\JokoAurkezpena.bmp"
#define NOLAJOKATU ".\\img\\NolaJokatu3.bmp"
#define LETRAKARAUAK ".\\img\\HitzakOsatu.bmp"
#define ArgazkiOndo ".\\img\\ok.bmp"
#define ArgazkiGaizki ".\\img\\gaizki1.bmp"
#define HASIETXEA ".\\img\\Etxea.bmp"
#define HASIIZARRA ".\\img\\IzarraFondo.bmp"
#define HASIMARRUBI ".\\img\\MarrubiFondo.bmp"
#define HASITOMATE ".\\img\\TomateFondo.bmp"
#define HASIORDULARIA ".\\img\\OdulariFondo.bmp"
#define HASIZERUA ".\\img\\ZeruaFondo.bmp"
#define HASITXUPATXUS ".\\img\\TxupatxusFondo.bmp"
#define HASITXAKURRA ".\\img\\TxakurFondo.bmp"
#define DESERTUA ".\\img\\Fondoa.bmp"
#define HIZKIZOPAAURKEZPENA ".\\img\\HizkiZopa.bmp"
#define HIZKIZOPAARAUAK ".\\img\\HizkiZopa2.bmp"
#define MAPAZENBAKIAK ".\\img\\mapa1-2.bmp"
#define ONDO ".\\sound\\Ondo.wav"
#define GAIZKI ".\\sound\\Gaizki.wav"

void hasiu();
void ZureIzena();
void hasi();
void menuletrak();
void nolajokatuletrak();
int hasijokoa();

int hasietxea();
int hasiizarra();
int hasimarrubi();
int hasitomate();
int hasiordularia();
int hasizerua();
int hasitxupatxus();
int hasitxakurra();
int hitzakeginda();

int hasisopadeletras();
int Arauak();
int sopadeletras();

int Bukaera();
int bigarrenuhartea();

#endif
