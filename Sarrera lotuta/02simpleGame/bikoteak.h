#ifndef bikoteak_H
#define bikoteak_H
#include "ourTypes.h"
#define JOKOA_ADINA ".\\img\\Sarrera ofi2.bmp"
#define KARTA_1 ".\\img\\pony+fondo.bmp"
#define KARTA_2 ".\\img\\dragon+fondo.bmp"
#define KARTA_3 ".\\img\\txoria+fondo.bmp"
#define KARTA_4 ".\\img\\karta4.bmp"
#define KARTA_5 ".\\img\\karta5.bmp"
#define KARTA_6 ".\\img\\karta6.bmp"
#define FONDO_SUMENDI ".\\img\\fondo-volcan.bmp"
#define NIBELA1 ".\\img\\fondofuego.bmp"
#define HURRENGONIBELA ".\\img\\hurrengonibela.bmp"
#define IRABAZITA ".\\img\\irabazifondo.bmp"
#define Mapa5 ".\\img\\mapa4-5.bmp"
#define GAIZKI ".\\sound\\Gaizki.wav"
#define ONDO ".\\sound\\Ondo.wav"
void Ondosum();
void Gaizkisum();
void sumhasiera();
void arauaks();
void arauaks2();
void bikoteakaurkitu();
int zuzena(int l);
void gaizki(int l);
void kartakmarraztu();
int bikoitia(int z);
int zenbatAldiz(int b[], int dim, int elementu);
int zenbatAldiz2(int dim, int elementu);
void atzera();
void samaiera(int puntuak);
void bostgarrenuhartea();
#endif