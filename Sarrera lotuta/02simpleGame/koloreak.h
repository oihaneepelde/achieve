#ifndef KOLOREAK
#define KOLOREAK

#define ONGI_ETORRI_MEZUA "ACHIEVE"
#define SarreraH ".\\img\\KOLOREAK (2).bmp"
#define Jarraitu ".\\img\\Sarrera ordezkoa 2.bmp"
#define Ikasi ".\\img\\ikasi.bmp"
#define Extra ".\\img\\Extra.bmp"
#define Expli1 ".\\img\\Sarrera 3.bmp"
#define JokoaIm ".\\img\\jokoaimportante.bmp"
#define Ikasi21 ".\\img\\jokoa2 1.bmp"
#define Ikasi22 ".\\img\\jokoa2 2.bmp"
#define Ikasi23 ".\\img\\jokoa2 3.bmp"
#define Ikasi24 ".\\img\\jokoa2 4.bmp"
#define Ikasi25 ".\\img\\jokoa2 5.bmp"
#define Ikasi26 ".\\img\\jokoa2 6.bmp"
#define Ikasi27 ".\\img\\jokoa2 7.bmp"
#define Ikasi28 ".\\img\\jokoa2 8.bmp"
#define Ikasi29 ".\\img\\jokoa2 9.bmp"
#define Ikasi210 ".\\img\\jokoa2 10.bmp"
#define Ikasi221 ".\\img\\Tranpa1.bmp"
#define Ikasi222 ".\\img\\Tranpa2.bmp"
#define Ikasi223 ".\\img\\Tranpa3.bmp"
#define Ikasi224 ".\\img\\Tranpa4.bmp"
#define Ikasi225 ".\\img\\Tranpa5.bmp"
#define Ikasi226 ".\\img\\Tranpa6.bmp"
#define Ikasi227 ".\\img\\Tranpa7.bmp"
#define Ikasi228 ".\\img\\Tranpa8.bmp"
#define Ikasi229 ".\\img\\Tranpa9.bmp"
#define Ikasi2210 ".\\img\\Tranpa10.bmp"
#define Tik ".\\img\\tik.bmp"
#define Tik2 ".\\img\\tik2.bmp"
#define Tik3 ".\\img\\tik3.bmp"
#define Tik4 ".\\img\\tik4.bmp"
#define Tik5 ".\\img\\tik5.bmp"
#define Tik6 ".\\img\\tik6.bmp"
#define Tik7 ".\\img\\tik7.bmp"
#define Tik8 ".\\img\\tik8.bmp"
#define Tik9 ".\\img\\tik9.bmp"
#define Tik10 ".\\img\\tik10.bmp"
#define Ekix1 ".\\img\\Gaixkiz.bmp"
#define Ekix2 ".\\img\\GaixkiB.bmp"
#define Ekix3 ".\\img\\GaixkiBel.bmp"
#define Ekix4 ".\\img\\GaixkiU.bmp"
#define Ekix5 ".\\img\\GaixkiL.bmp"
#define Ekix6 ".\\img\\GaixkiG.bmp"
#define Ekix7 ".\\img\\GaixkiM.bmp"
#define Ekix8 ".\\img\\GaixkiMo.bmp"
#define Ekix9 ".\\img\\GaixkiH.bmp"
#define Ekix10 ".\\img\\GaixkiA.bmp"
#define Hurrengoa ".\\img\\Hurrengoa.bmp"
#define Amaitu ".\\img\\Amaitu.bmp"
#define SarreraB ".\\img\\Sarrera 4 orokorra2.bmp"
#define SarreraB1 ".\\img\\Sarrera 4 orokorra1.bmp"
#define Mapa4 ".\\img\\Mapa nire.bmp"
#define GAIZKI ".\\sound\\Gaizki.wav"
#define ONDO ".\\sound\\Ondo.wav"
#define Zuria ".\\sound\\Zuria.wav"
#define Beltza ".\\sound\\Beltza.wav"
#define Laranja ".\\sound\\Laranja.wav"
#define Marroia ".\\sound\\Marroia.wav"
#define Horia ".\\sound\\Horia.wav"
#define Arrosa ".\\sound\\Arrosa.wav"
#define Morea ".\\sound\\Morea.wav"
#define Gorria ".\\sound\\Gorria.wav"
#define Urdina ".\\sound\\Urdina.wav"
#define Berdea ".\\sound\\Berdea.wav"
#define Txina ".\\sound\\China.wav"

int koloreak();
void Expli();
void borobilak();
void Ondo();
void Gaizki();
void Ikastera();
void explikazioa();
int Jolastera();
int Gaizki2();
int Jolastera2();
int Gaizki3();
int Jolastera3();
int Gaizki4();
int Jolastera4();
int Gaizki5();
int Jolastera5();
int Gaizki6();
int Jolastera6();
int Gaizki7();
int Jolastera7();
int Gaizki8();
int Jolastera8();
int Gaizki9();
int Jolastera9();
int Gaizki10();
int Jolastera10();
int Gaizki11();
void Sarrera4(int puntuak);


#endif