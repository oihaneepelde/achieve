#ifndef GAME10_H
#define GAME10_H

#include "ourTypes.h"

void jokoaAurkeztu(void);
int jokoAmaierakoa(EGOERA egoera);

int menuumeak();
int menugazteak();
int menuhelduak();
int hasiu();
int hasig();
int hasih();
int azalpenakumeak();
int kredituakumeak();
int azalpenakgazteak();
int kredituakgazteak();
int azalpenakhelduak();
int kredituakhelduak();

int BUKAERA_irudiaBistaratu();
#endif
